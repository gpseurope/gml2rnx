# **GeodesyML to Rinex** software:

This software has been developped to fill station metadata from GLASS-API or M3G-API into RINEX header.  
The main application is to complete RINEX3 header of file that was directly uploaded from receiver.  
This new RINEX file can be achived in specified data repository architecture.  

The sofware is designed to be efficient and ready to use.  

## Fill RINEX header from M3G, DGW or GeodesyML file metadata.  

### Default modified sections:
* RUN BY
* MARKER NAME
* MARKER NUMBER
* OBSERVER / AGENCY
* APPROX POSITION XYZ
* ANTENNA: DELTA H/E/N

### Optionnal modified sections:
* ANT # / TYPE
* REC # / TYPE / VERS
* \# OF SATELLITES
* PRN / # OF OBS
* COMMENT 

## Other functions:
* Decimate and concatenate multiple Rinex files to 30s daily files.  
* Compress or deflate Rinex files using Hatanaka and Gzip.  
Rinex2 to Rinex3 conversion and vice versa is **not** supported.  
* Save output Rinex files into specified repository architecture.  
* View the observation summary of Rinex file.  
This can be inserted in Rinex header.  
* Download the latest GeodesyML file.  
This can be archived into specified repository architecture. 

# Usage:
    gml2rnx.py          [-h] [--Version] [--Quiet] 
                        [--GeodesyML {DGW, M3G} or GeodesyML path] 
                        [--GeodesyMLoutDirectory GeodesyML store path] 
                        [--GeodesyMLdownloadList Comma separated site list for GeodesyML download] 
                        [--InputPath Input directory path to process Rinex files]
                        [--OutType {rnx,rnz,crx,crz}]  
                        [--OutDirectory new Rinex destination directory]
                        [--ForceOverwrite] [--Decimate] [--Summary] [--SummaryPrint] [--ChecksumMd5] 
                        [--OverwriteReceiver] [--OverwriteAntenna] [--KeepXYZ] [--KeepEccentricity] [--KeepMarkerName] 
                        [--Comment "max 60 characters string" ["max 60 characters string" ...]] 
                        [--RunBy {dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string] 
                        [--Observer {dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string] 
                        [--Agency {dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string] 
                        [--RemoveHistory Overwrite metadata Rinex header]
                        [--SiteEnd Last 5ch of 9ch Sitename] 
                        [--ForcedMetadata "RUN BY, MARKER NAME, MARKER NUMBER, OBSERVER, AGENCY, REC #, REC TYPE, REC VERS, ANT #, ANT TYPE, X, Y, Z, H, E, N"] 
                        [Rinex_file [Rinex_file ...]] 

    Positional arguments:
    Rinex_file          Rinex2 or Rinex3 files to be processed.
                        Hatanaka and Gzip compression supported.
                        For huge files processing, please use --InputPath option.

    Optional arguments:
    -h, --help
                        Show this help message and exit.
    --Version, -v
                        Show program's version number and exit.
    --Quiet, -q
                        Disable notifications.
                        Generated output files will be printed on stdout, errors on stderr.
    --GeodesyML, -g  {DGW, M3G} or GeodesyML path
                        GeodesyML path or repository to get metadata.
                        By default it will retrieve metadata from DGW GLASS-API.
    --GeodesyMLoutDirectory, -gd  GeodesyML store path
                        Output directory path to store GeodesyML.
                        Date POSIX format accepted for repository architecture.
                        End the path with "/#.gz" to compress GeodesyML files with Gzip.
    --GeodesyMLdownloadList, -gl  Comma separated site list for GeodesyML download
                        Download GeodesyML to specified direcory, nothing else will be done.
                        Site should be in 9 characters, otehrwise use the --SiteEnd option.
                        To retrieve all stations from a network, use "%" special key before network name like "%RENAG".
                        --GeodesyMLoutDirectory option is mandatory, --GeodesyML and --SiteEnd options are optional.
                        ie: ./gml2rnx.py -g M3G -gl "NICE00FRA,GINA00FRA,CLFD00FRA" -gd ./%Y/#.gz
    --InputPath, -i     Input directory path to process Rinex files.
                        Wildcards is allowed using quotes.
                        ie: ./gml2rnx.py -i "/data/2024/???/SITE00FRA*"
    --OutType, -t  {rnx,rnz,crx,crz}
                        Select the output Rinex compression type.
                        By default it will use the same compression as input file.
    --OutDirectory, -o new Rinex destination directory
                        Output directory path to write new Rinex files.
                        Date POSIX format accepted for repository architecture.
                        #INTERVAL and #DURATION special key and also their lowercase values can be use to complete the path.
                        ie: ./gml2rnx.py -o /DATA/#interval_#duration/%Y/%j/ SITE00FRA*.crx.gz
    --ForceOverwrite, -f
                        Overwrite output Rinex file.
                        By default, ".new" extension will be added.
                        Caution: if no output specified, it will overwrite source Rinex file.
    --Decimate, -d
                        Decimate and concatenate Rinex files to daily 30sec files
    --Summary, -s
                        Compute satellites observations summary.
                        Add # OF SATELLITES and PRN / # OF OBS section in Rinex header.
    --SummaryPrint, -sp
                        Compute and print satellites observations summary.
                        Nothing else will be done except when using --Summary option in the same time.
    --ChecksumMd5, -cm
                        Compute and print uncompressed and compressed MD5 checksum.
                        Output will be: FILE_PATH,UNCOMPRESSED_MD5,COMPRESSED_MD5  
    --OverwriteReceiver, -or
                        Overwrite receiver metadata in Rinex header.
                        By default, the REC # / TYPE / VERS section is not modified.
    --OverwriteAntenna, -oa  
                        Overwrite antenna metadata in Rinex header.
                        By default, the ANT # / TYPE section is not modified.
    --KeepXYZ, -kx
                        Keep the aproximate position coordinates in Rinex header.
                        By default, the APPROX POSITION XYZ section is modified.
    --KeepEccentricity, -ke
                        Keep the antenna eccentricity in Rinex header.
                        By default, the ANTENNA: DELTA H/E/N section is modified.
    --KeepMarkerName, -kmn
                        Keep the marker name in Rinex header.
                        By default it takes 9 characters site name.
    --RemoveHistory, -rh 
                        Overwrite metadata Rinex header without saving history.
                        By default, original line is inserted in comment after modification in the header.
    --Comment, -c  "max 60 characters string" ["max 60 characters string" ...]
                        Add comment line in Rinex header
    --RunBy, -rb  {dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string
                        Select the RUN BY Rinex header section from metadata list.
                        By default it takes primary datacenter from metadata.
                        You can also fill custom string, empty value will keep Rinex header entry.
    --Observer, -ob  {dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string
                        Select the OBSERVER Rinex header section from metadata list.
                        By default it takes on-site primary contact name.
                        You can also fill custom string, empty value will keep Rinex header entry.
    --Agency, -ag  {dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string
                        Select the AGENCY Rinex header section from metadata list.
                        By default it takes responsible agency abbreviation.
                        You can also fill custom string, empty value will keep Rinex header entry.
    --SiteEnd, -se  Last 5ch of 9ch Sitename
                        For GeodesyML download, 9 characters sitename is requied.
                        This option is needed to complete the 4 characters written in Rinex2 filename.
                        By default, 00FRA will be used.
    --ForcedMetadata, -fm  "RUN BY, MARKER NAME, MARKER NUMBER, OBSERVER, AGENCY, REC #, REC TYPE, REC VERS, ANT #, ANT TYPE, X, Y, Z, H, E, N"
                        Force Rinex header metadata values.
                        Be carefull, MARKER NAME section will also change the output filename even multiples files are processed.
                        Values are comma separated, empty string will keep Rinex header entry.
                        Use of "keep" parameter will NOT modifiy header metadata.
                        This option implies no M3G, DGW or GeodesyML metadata retrievement.
                        --GeodesyML, --OverwriteReceiver, --OverwriteAntenna, --KeepXYZ, --KeepEccentricity, --KeepMarkerName, --RunBy, --Observer, and --Agency options will be ignored.
                        ie: ./gml2rnx.py -fm "RENAG Automatic,,,OREME-Team,RESIF,,,,CR520021307,ASH701945D_M    NONE,4591090.1844,258415.2417,4406201.0377,0,0,0" SITE00FRA*.crx.gz

# Requirements:
## Mandatory
* Python 3  
Software has been tested with Python version 3.6, 3.7 and 3.10 on linux OS.
* Compact Rinex inflate/deflate tool based on Hatanaka compression.  
You can install RNXCMP system program from https://terras.gsi.go.jp/ja/crx2rnx.html .   
Or install hatanaka Python library using `pip3 install hatanaka` (NB: slower than RNXCMP).  
## Optionnal
* Multithread Gzip inflate/deflate tool.  
By default it will use Gzip Python library witch is already installed.  
To speedup the tool you can install the pgzip Python library using `pip3 install pgzip`.  

# Tutorial:
## 1. Complete Rinex3 header information from DGW station info metadata
`./gml2rnx.py examples/NICE00FRA_R_20230980000_01D_30S_MO.crx.gz`  
The *NICE00FRA_R_20230980000_01D_30S_MO.crx* compact Rinex3.04 file has been created and sent by a Trimble NetR9 receiver.  
This will create a new file *examples/NICE00FRA_R_20230980000_01D_30S_MO.crx.gz.new* containing informations from GLASS-API.  
## 2. Complete and archive Rinex3 file sent by receiver  
`./gml2rnx.py examples/GINA00FRA_R_20230980000_01D_30S_MO.crx --RunBy "RENAG DC" --Comment "CC-BY-4.00 DATA Licence" "DOI: 10.15778/resif.rg" --OutType crz --OutDirectory examples/archive/%Y/%j/`  
The *GINA00FRA_R_20230980000_01D_30S_MO.crx* compact Rinex3.03 file has been created and sent by a Leica GR25 receiver.  
The path */archive/2023/098/* is automaticaly created according to the filename date.  
The gzip compressed output file is archived in *examples/archive/2023/098/GINA00FRA_R_20230980000_01D_30S_MO.crx.gz*
## 3. Decimate 1s/1h files to 30s/1d file
`./gml2rnx.py examples/1S_1H_RINEX2/* --SiteEnd 00FRA --Decimate -t rnx -o examples`  
The source files are issued from RENAG Data Center Rinex2 archive.  
The sitename in Rinex2 format is in 4 characters, --SiteEnd option is recommended to get the correct metadata in case of homonym site.  
This will create the *examples/clfd0980.23o* 30s daily Rinex2 file.  
## 4. Fix Rinex3 data stored in Rinex2 filename with homemade metadata
`./gml2rnx.py examples/sep20980.23d.Z --ForcedMetadata "Project X,SEP200FRA,NONE,OCA Team,Geoazur lab,,,,123456789,AS-ANT2BCAL     NONE,,,,,,"`  
The *sep20980.23o* Rinex3.04 has been created by a Septentrio Mosaic-go receiver.  
The filename is like Rinex2 format but the content is in Rinex3 format.  
The INTERVAL header section is missing in this file, this will be automaticaly inserted in the output file.  
The station metadata is not available on DGW or M3G API.  
The --ForcedMetadata option is used to fill manualy the Rinex header sections.  
This will create a new file in Rinex3 filename format: *examples/SEP200FRA_R_20230980000_01D_30S_MO.crx.gz*.  
## 5. Download GeodesyML files from M3G
`./gml2rnx.py -g M3G -gl AMST -gd examples/GeodesyML/#.gz`  
On M3G there is AMST00AUT and AMST00NLD station availlable.  
This command will not work because it will try to download AMST00FRA.  
The -se (--SiteEnd) option can used to build the 9char from AMST 4char site.  
We encourage to use 9char sitename directly to avoid troubles:  
`./gml2rnx.py -g M3G -gl AMST00NLD,AMST00AUT -gd examples/GeodesyML/#.gz`  
These commands will download and compress the files:  
*examples/GeodesyML/AMST00NLD_20220428T00:00:00Z.xml.gz*  
*examples/GeodesyML/AMST00AUT_20221110T00:00:00Z.xml.gz*  
Now you can use the option `-g examples/GeodesyML/` to get metadata from local GeodesyML archive.
## 6. Get observation summary
`./gml2rnx.py examples/GINA00FRA_R_20230980000_01D_30S_MO.crx -sp`  
This will show the number of each observation type on each satellite constellation:  
```bash
NB OBS: 2880 / 2880 = 100 %  
  
PRN, C1C, L1C, S1C, C2S, L2S, S2S, C2W, L2W, S2W, C5Q, L5Q, S5Q  
G01, 798, 798, 798, 798, 798, 798, 798, 798, 798, 798, 798, 798  
G02, 850, 850, 850,   0,   0,   0, 843, 843, 843,   0,   0,   0  
G03, 841, 841, 841, 841, 841, 841, 841, 841, 841, 841, 841, 841  
G04, 760, 760, 760, 759, 759, 759, 758, 758, 758, 749, 749, 749  
G05, 910, 909, 910, 899, 899, 899, 908, 907, 908,   0,   0,   0  
G06, 900, 897, 900, 895, 895, 895, 897, 894, 897, 899, 899, 899  
G07, 723, 723, 723, 723, 723, 723, 723, 723, 723,   0,   0,   0  
G08, 722, 722, 722, 722, 722, 722, 722, 722, 722, 722, 722, 722  
G09, 819, 819, 819, 819, 819, 819, 819, 819, 819, 814, 814, 814  
G10, 870, 870, 870, 869, 869, 869, 869, 869, 869, 854, 854, 854  
G11, 982, 982, 982, 980, 980, 980, 979, 979, 979, 982, 982, 982  
G12, 819, 819, 819, 819, 819, 819, 819, 819, 819,   0,   0,   0  
G13, 801, 801, 801,   0,   0,   0, 800, 800, 800,   0,   0,   0  
G14, 906, 906, 906, 904, 904, 904, 904, 904, 904, 904, 904, 904  
G15, 732, 731, 732, 731, 731, 731, 731, 731, 731,   0,   0,   0  
G16, 858, 856, 858,   0,   0,   0, 856, 855, 856,   0,   0,   0  
G17, 998, 998, 998, 998, 998, 998, 998, 998, 998,   0,   0,   0  
G18, 715, 715, 715, 715, 715, 715, 715, 715, 715, 715, 715, 715  
G19, 975, 974, 975,   0,   0,   0, 974, 973, 974,   0,   0,   0  
G20, 892, 891, 892,   0,   0,   0, 890, 889, 890,   0,   0,   0  
G21, 855, 855, 855,   0,   0,   0, 855, 855, 855,   0,   0,   0  
G23, 957, 955, 957, 957, 957, 957, 957, 955, 957, 948, 948, 948  
G24, 804, 804, 804, 804, 804, 804, 804, 804, 804, 804, 804, 804  
G25, 829, 829, 829, 829, 829, 829, 829, 829, 829, 829, 829, 829  
G26, 709, 709, 709, 709, 709, 709, 709, 709, 709, 705, 705, 705  
G27, 800, 800, 800, 800, 800, 800, 800, 800, 800, 800, 800, 800  
G28, 883, 881, 883, 873, 873, 873, 876, 875, 876, 863, 863, 863  
G29, 776, 776, 776, 776, 776, 776, 776, 776, 776,   0,   0,   0  
G30, 776, 776, 776, 775, 775, 775, 775, 775, 775, 776, 776, 776  
G31, 941, 941, 941, 932, 932, 932, 936, 936, 936,   0,   0,   0  
G32, 990, 990, 990, 987, 987, 987, 989, 989, 989, 989, 989, 989  

PRN, C1C, L1C, S1C, C2P, L2P, S2P, C2C, L2C, S2C  
R01, 830, 830, 830, 829, 829, 829, 830, 828, 830  
R02, 677, 677, 677, 667, 667, 667, 668, 667, 668  
R03, 984, 984, 984, 973, 972, 973, 972, 972, 972  
R04,1009,1009,1009,1008,1008,1008,1009,1008,1009  
R05, 993, 993, 993, 992, 991, 992, 992, 991, 992  
R06, 504, 504, 504,   0,   0,   0,   0,   0,   0  
R07, 871, 871, 871, 870, 870, 870, 871, 871, 871  
R08, 872, 872, 872, 871, 871, 871, 872, 872, 872  
R09, 776, 776, 776, 775, 775, 775, 775, 775, 775  
R10, 735, 735, 735,   0,   0,   0,   0,   0,   0  
R11, 704, 704, 704, 701, 701, 701, 702, 702, 702  
R12, 908, 908, 908, 908, 908, 908, 908, 908, 908  
R13, 842, 842, 842, 842, 842, 842, 842, 842, 842  
R14,1099,1099,1099,1099,1099,1099,1099,1099,1099  
R15, 986, 986, 986, 986, 985, 986, 986, 986, 986  
R16, 714, 714, 714, 713, 713, 713, 714, 713, 714  
R17, 931, 931, 931, 929, 929, 929, 930, 929, 930  
R18,1002,1002,1002,1002,1002,1002,1002,1002,1002  
R19, 937, 937, 937, 937, 937, 937, 937, 937, 937  
R20, 905, 905, 905, 905, 905, 905, 905, 905, 905  
R22, 702, 702, 702, 702, 702, 702, 702, 702, 702  
R23, 773, 773, 773,   0,   0,   0,   0,   0,   0  
R24, 819, 819, 819, 815, 815, 815, 817, 817, 817  

PRN, C1C, L1C, S1C, C5Q, L5Q, S5Q, C7Q, L7Q, S7Q, C8Q, L8Q, S8Q  
E01, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219  
E02,1019,1019,1019,1019,1019,1019,1019,1019,1019,1019,1019,1019  
E03,1167,1167,1167,1167,1167,1167,1167,1167,1167,1167,1167,1167  
E04, 328, 328, 328, 328, 328, 328, 328, 328, 328, 328, 328, 328  
E05, 856, 856, 856, 856, 856, 856, 856, 856, 856, 856, 856, 856  
E07, 811, 811, 811, 811, 811, 811, 811, 811, 811, 810, 810, 810  
E08,1124,1124,1124,1124,1124,1124,1124,1124,1124,1124,1124,1124  
E09, 679, 679, 679, 679, 679, 679, 679, 679, 679, 679, 679, 679  
E10, 190, 190, 190, 190, 190, 190, 190, 190, 190, 190, 190, 190  
E11, 484, 484, 484, 484, 484, 484, 484, 484, 484, 484, 484, 484  
E12, 155, 155, 155, 155, 155, 155, 155, 155, 155, 155, 155, 155  
E13,1016,1016,1016,1016,1016,1016,1016,1016,1016,1016,1016,1016  
E15, 971, 971, 971, 971, 971, 971, 971, 971, 971, 971, 971, 971  
E19, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200  
E21, 448, 448, 448, 445, 445, 445, 443, 443, 443, 445, 445, 445  
E24, 695, 695, 695, 695, 695, 695, 695, 695, 695, 695, 695, 695  
E25, 861, 861, 861, 860, 860, 860, 861, 861, 861, 861, 861, 861  
E26, 758, 758, 758, 758, 758, 758, 758, 758, 758, 758, 758, 758  
E27, 685, 685, 685, 684, 684, 684, 684, 684, 684, 684, 684, 684  
E30, 918, 918, 918, 917, 917, 917, 917, 917, 917, 917, 917, 917  
E31, 528, 528, 528, 527, 527, 527, 527, 527, 527, 527, 527, 527  
E33, 468, 468, 468, 468, 468, 468, 468, 468, 468, 468, 468, 468  
E34,1088,1088,1088,1088,1088,1088,1088,1088,1088,1088,1088,1088  
E36, 822, 822, 822, 822, 822, 822, 822, 822, 822, 822, 822, 822  
```
# Performance:
Tested on Linux server with 8 Intel(R) Xeon(R) D-2123IT CPU @ 2.20GHz and 32GB Memory.  
Each commands have been runned 3 times and we took the minimum time of processing.  
Pgzip python library installed, hatanaka python library not installed.  

## Compression
*GINA00FRA_R_20230980000_01D_30S_MO.crx* has been uncompressed previously.  
### RNX to CRZ:
```bash
time $(RNX2CRX -f examples/GINA00FRA_R_20230980000_01D_30S_MO.rnx ; gzip -f examples/GINA00FRA_R_20230980000_01D_30S_MO.crx)

real    0m0.562s
user    0m0.553s
sys     0m0.010s

time ./gml2rnx.py -q -f -fm keep -t crz examples/GINA00FRA_R_20230980000_01D_30S_MO.rnx
examples/GINA00FRA_R_20230980000_01D_30S_MO.crx.gz

real    0m0.343s
user    0m1.098s
sys     0m0.044s
```

### CRZ to RNX:
```bash
time $(gzip -d -f examples/GINA00FRA_R_20230980000_01D_30S_MO.crx.gz ; CRX2RNX -f examples/GINA00FRA_R_20230980000_01D_30S_MO.crx)

real    0m0.221s
user    0m0.201s
sys     0m0.020s

time ./gml2rnx.py -q -f -fm keep -t rnx examples/GINA00FRA_R_20230980000_01D_30S_MO.crx.gz
examples/GINA00FRA_R_20230980000_01D_30S_MO.rnx

real    0m0.301s
user    0m0.330s
sys     0m0.042s
```
## Decimation
All 1s/1h Rinex files have been uncompressed previously.  
Gfzrnx software installed version is 1.13-7761.  

```bash
time gfzrnx -q -finp examples/1S_1H_RINEX2/*.23o -vo 2 -smp 30 -fout examples/::RX2::

real    0m17.532s
user    0m17.483s
sys     0m0.048s

time ./gml2rnx.py -q -f -fm keep -d -o examples/ examples/1S_1H_RINEX2/*.23o
examples/clfd098a.23o

real    0m0.838s
user    0m0.770s
sys     0m0.068s

```

