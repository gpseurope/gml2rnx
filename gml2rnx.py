#!/usr/bin/python3

# Copyright
#  Maurin VIDAL (maurin.vidal@geoazur.unice.fr)
#  CNRS / OCA / Geoazur Lab.
VERSION = "1.2.4"

import http.client
from datetime import datetime,timedelta
from sys import stderr
from os import path,makedirs,stat
from glob import glob
import argparse
from zipfile import ZipFile
from io import BytesIO
from hashlib import md5
from json import loads
from subprocess import Popen,PIPE
#use pgzip or gzip library
try:
    import pgzip 
    pgzip_BS=2**15
except ModuleNotFoundError:
    pgzip_BS=0
import gzip
#use hatanaka library or RNXCMP system program
try:
    import hatanaka
    rnx2crx=crx2rnx='hatanaka lib'
except ModuleNotFoundError:
    from shutil import which
    rnx2crx=which("RNX2CRX")
    crx2rnx=which("CRX2RNX")

#check memory usage
#from memory_profiler import profile
#@profile

def get_rinex(file_obj):
    ######################################
    # Get Rinex informations and content #
    ######################################
    metadata=dict()
    metadata["decimate"]=False
    metadata["hatanaka_decompress"]=False
    metadata["_R_"]="_R_"
    metadata["new_site"]=False
    metadata["name"]=file_obj.name
    metadata["bname"]=path.basename(metadata["name"])
    #check rinex2 filename format
    if metadata["bname"][7].lower() in ('0','1','2','3','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x') and metadata["bname"][8] == '.' and metadata["bname"][11:].lower() in ('o','d','o.z','o.gz','d.z','d.gz'):
        #get rinex type
        if metadata["bname"][-1:].lower() == 'o':
            metadata["type"]='rnx2'
        elif metadata["bname"][-1:].lower() == 'd':
            metadata["type"]='crx2'
        elif metadata["bname"][-4:].lower() == 'd.gz' or metadata["bname"][-3:].lower() == 'd.z':
            metadata["type"]='crz2'
        elif metadata["bname"][-4:].lower() == 'o.gz' or metadata["bname"][-3:].lower() == 'o.z':
            metadata["type"]='rnz2'
        #get site and duration from file name
        metadata["site"]=metadata["bname"][:4].upper()+args.SiteEnd
        if metadata["bname"][7] in ('0','1','2','3'):
             metadata["duration"]='01D'
        else:
             metadata["duration"]='01H'
    #check rinex3 filename format
    elif metadata["bname"][9:12] in ('_R_','_S_') and metadata["bname"][30:35] in ('S_MO.','S_GO.','Z_MO.','Z_GO.') and metadata["bname"][35:].lower() in ('rnx','crx','rnx.gz','crx.gz'):
        #get rinex type
        if metadata["bname"][-4:].lower() == '.rnx':
            metadata["type"]='rnx3'
        elif metadata["bname"][-4:].lower() == '.crx':
            metadata["type"]='crx3'
        elif metadata["bname"][-7:].lower() == '.crx.gz':
            metadata["type"]='crz3'
        elif metadata["bname"][-7:].lower() == '.rnx.gz':
            metadata["type"]='rnz3'
        #get site, first observation, duration and interval from file name
        metadata["site"]=metadata["bname"][:9]
        metadata["date"]=datetime.strptime(metadata["bname"][12:23],"%Y%j%H%M")
        metadata["duration"]=metadata["bname"][24:27].upper()
        metadata["interval"]=int(metadata["bname"][28:30])
        #get Receiver or Stream rinex data source
        metadata["_R_"]=metadata["bname"][9:12]
    #check skipping file
    elif metadata["bname"][-4:] == '.new':
        if not args.Quiet: print('NOTICE: Skip {}.'.format(metadata["name"]), file=stderr, flush=True)
        return None
    else:
        print('ERROR: {} do not follow Rinex observation filename convention.'.format(metadata["name"]), file=stderr, flush=True)
        return None
    #uncompress and read rinex file
    if metadata["type"][2] == 'z':
        try:
            metadata["content"]=gzip.decompress(file_obj.read()).decode('utf-8', 'replace')
        except OSError as error : 
            #Use gunzip system command instead of gzip python library
            #ie: gzip library do not work with lzw compression 
            if "Not a gzipped file" in str(error):
                gzip_prog = Popen(['gunzip', '-c', metadata["name"]], stdout=PIPE)
                metadata["content"] = gzip_prog.communicate()[0].decode('utf-8', 'replace')
            else:
                print(error) 
                return None  
    else:
        metadata["content"]=file_obj.read().decode('utf-8', 'replace')
    #check Rinex Format in header
    if metadata["content"][60:66] not in ("CRINEX","RINEX "):
        print('ERROR: {} is not Rinex or compact Rinex observation file.'.format(metadata["name"]), file=stderr, flush=True)
        return None
    #check dos or unix new line text format
    if '\r\n' in metadata["content"][:82]:
        #print("DOS")
        metadata["nl"]='\r\n'
    else:
        #print("UNIX")
        metadata["nl"]='\n'
    #get rinex end of header string index
    metadata["header_index"]=metadata["content"].find("END OF HEADER",0,25000)
    if metadata["header_index"] == -1:
        print("ERROR: Rinex file malformated: cannot find 'END OF HEADER'", file=stderr, flush=True)
        return None
    #relocate header index just after new line
    metadata["header_index"]=metadata["content"].find(metadata["nl"],metadata["header_index"],metadata["header_index"]+22)+len(metadata["nl"])
    #get Rinex version from header
    if metadata["type"][0]=='r':
        metadata["version"]=float(metadata["content"][5:9])
    else:
        if metadata["nl"]=='\r\n':
            metadata["version"]=float(metadata["content"][167:171])
        else:
            metadata["version"]=float(metadata["content"][165:169])
    #get the rinex2 first observation (already written in rinex3 filename)
    if metadata["type"][3]=='2':
        #find first observation in header
        index=metadata["content"].find("TIME OF FIRST OBS",0,metadata["header_index"])
        if index == -1:
            print("ERROR: Rinex file malformated: cannot find 'TIME OF FIRST OBS'", file=stderr, flush=True)
            return None
        metadata["date"]=datetime.strptime(metadata["content"][index-58:index-25],"%Y    %m    %d    %H    %M   %S")
    #get the rinex2 observation interval (already written in rinex3 filename)
    if metadata["type"][3]=='2':
        #find interval in header
        index=metadata["content"].find("INTERVAL",0,metadata["header_index"])
        if index!=-1:
            metadata["interval"]=int(metadata["content"][index-56:index-54])
        else:
            #inverval not found in header
            if not args.Quiet: print("WARNING: Rinex file malformated: cannot find 'INTERVAL' in header:", file=stderr, flush=True)
            if metadata["duration"]=='01D':
                if not args.Quiet: print("Set interval to 30s according to filename session number.", file=stderr, flush=True)
                metadata["interval"]=30
            else:
                if not args.Quiet: print("Set interval to 1s according to filename session number.", file=stderr, flush=True)
                metadata["interval"]=1
    return metadata


def get_M3G_geodesyML(site_9char):
    ##########################
    # Get GeodesyML from M3G #
    ##########################
    #define HTTPS URL API
    m3g = http.client.HTTPSConnection("gnss-metadata.eu", timeout=2)
    #define GET request with sitename
    m3g.request('GET', '/v1/sitelog/exportxml?id='+site_9char)
    #run request and print error exit if http status is not 200
    response = m3g.getresponse()
    if response.status != 200: 
        print('ERROR: Cannot retrieve {} GeodesyML from M3G API, ERROR{}: {}'.format(site_9char,response.status, response.reason), file=stderr, flush=True)
        return None
    #get geodesyML contents
    return response.read().decode('utf-8', 'replace')

def get_DGW_geodesyML(site_9char):
    ##########################
    # Get GeodesyML from DGW #
    ##########################
    #define HTTPS URL API
    dgw = http.client.HTTPSConnection("gnssdata-epos.oca.eu", timeout=2)
    #define GET request with sitename
    dgw.request('GET', '/GlassFramework/webresources/log/geodesyml/'+site_9char[:4])
    #run request and print error exit if http status is not 200
    response = dgw.getresponse()
    if response.status != 200: 
        print('ERROR: Cannot retrieve {} GeodesyML from DGW API, ERROR{}: {}'.format(site_9char,response.status, response.reason), file=stderr, flush=True)
        return None
    #get zipped geodesyML contents
    z=ZipFile(BytesIO(response.read()))
    zfile=list(filter(lambda f: site_9char in f, z.namelist()))[0]
    return z.read(zfile).decode('utf-8', 'replace')

def get_geodesyML(site_9char,pathML):
    ###########################
    # Get GeodesyML from path #
    ###########################
    #check path
    if path.isfile(pathML):
        #return file content
        if pathML[-3:]==".gz":
            return gzip.open(pathML).read().decode("utf-8","replace")
        else:
            return open(pathML, encoding="utf8", errors='replace').read()
    elif path.isdir(pathML):
        #list files beginning with the 9 characters sitename in the directory
        files=glob(path.join(pathML,site_9char+'*'))
        if files:
            #return last file founded content
            if files[-1][-3:]==".gz":
                return gzip.open(files[-1]).read().decode("utf-8","replace")
            else:
                return open(files[-1], encoding="utf8", errors='replace').read()
        else:
            print('ERROR: Cannot get {} GeodesyML file in {} directory.'.format(site_9char,pathML), file=stderr, flush=True)
            return None
    else:
        print('ERROR: {} is not a file or directory'.format(pathML), file=stderr, flush=True)
        return None
        
def get_DGW_network_sites(network):
    ##########################
    # Get site list from DGW #
    ##########################
    #define HTTPS URL API
    dgw = http.client.HTTPSConnection("gnssdata-epos.oca.eu", timeout=2)
    #define GET request with sitename
    dgw.request('GET', '/GlassFramework/webresources/stations/v2/network/'+network+'/short/csv')
    #run request and print error exit if http status is not 200
    response = dgw.getresponse()
    if response.status != 200: 
        print('ERROR: Cannot retrieve {} network sites from DGW API, ERROR{}: {}'.format(network,response.status, response.reason), file=stderr, flush=True)
        return None
    #get 9char_site list from contents
    return ','.join(list(map( lambda x : x.decode('utf-8', 'replace').split(',')[1].lstrip() , response.readlines()[1:])))

def get_M3G_network_sites(network):
    ##########################
    # Get site list from M3G #
    ##########################
    #define HTTPS URL API
    m3g = http.client.HTTPSConnection("gnss-metadata.eu", timeout=2)
    #define GET request with sitename
    m3g.request('GET', '/v1/network/view?id='+network)
    #run request and print error exit if http status is not 200
    response = m3g.getresponse()
    if response.status != 200: 
        print('ERROR: Cannot retrieve {} network sites from M3G API, ERROR{}: {}'.format(network,response.status, response.reason), file=stderr, flush=True)
        return None
    #get site list from json contents
    return ','.join(loads(response.read())["included"])

def get_metadata(geodesyML):
    ###################################
    # Parse GeodesyML to get metadata #
    ###################################
    #initialize a metadata dictionnary with an array of dict for antenna and receiver sessions
    metadata={ "geo:preparedBy" : None, "geo:datePrepared" : None, "geo:siteName" : None, "geo:iersDOMESNumber" : None, "geo:dataCenter" : None, "geo:ownerAgency" : None,"geo:ownerContact" : None, "geo:onSiteAgency" : None, "geo:onSiteContact" : None, "geo:X" : None, "geo:Y" : None, "geo:Z" : None, "receiver_tab" : [], "antenna_tab" : [] }
    #inititalise receiver and antenna current dictionnary
    rec_dict=dict()
    ant_dict=dict()
    #initialise flags showing geodesyML subsections
    rec_flag=False
    ant_flag=False
    xyz_flag=False
    own_flag=False
    sit_flag=False

    #loop for each geodesyML lines
    for line in geodesyML.split('\n'):
        #check subsections
        if "<geo:gnssReceiver>" in line:
            rec_flag=True
        if "</geo:gnssReceiver>" in line:
            rec_flag=False
            #insert curent receiver dictionnary into metadata["receiver_tab"]
            metadata["receiver_tab"].append(rec_dict)
            #reset current receiver dictionnary
            rec_dict=dict()
        if "<geo:gnssAntenna>" in line:
            ant_flag=True
        if "</geo:gnssAntenna>" in line:
            ant_flag=False
            #insert curent antenna dictionnary into metadata["antenna_tab"]
            metadata["antenna_tab"].append(ant_dict)
            #reset current antenna dictionnary
            ant_dict=dict()
        if "<geo:cartesianPosition>" in line:
            xyz_flag=True
        if "</geo:cartesianPosition>" in line:
            xyz_flag=False
        if "<geo:siteOwner" in line:
            own_flag=True
        if "</geo:siteOwner" in line:
            own_flag=False
        if "<geo:siteContact" in line:
            sit_flag=True
        if "</geo:siteContact" in line:
            sit_flag=False

        #get metadata when XML start & end tags contains in one line
        split_end=line.split("</")
        if len(split_end) == 2 and '<' in split_end[0]:
                #get variable name contains bettween '<' and '>' before '</'
                #srink variable name with contents before space delimiter
                var=split_end[0].split('>')[0].split('<')[1].split(' ')[0]
                #get value contains bettween '>' and '</'
                val=split_end[0].split('>')[1]
                #get data contain in CDATA array
                if "<![CDATA[" in val:
                    val=val.split("<![CDATA[")[1][:-2]
                #complete short date format
                if var[:8] == "geo:date" and len(val)==10:
                    val+="T00:00:00Z"

                #retrive all receiver metadata into current dict
                if rec_flag:
                    if var not in rec_dict:
                        rec_dict[var]=val
                    else:
                        rec_dict[var]+='+'+val
                #retrive all antenna metadata into current dict
                elif ant_flag:
                    ant_dict[var]=val
                #get XYZ positon in cartesianPosition subsection
                elif xyz_flag:
                    if var=="gml:pos":
                        metadata["geo:X"],metadata["geo:Y"],metadata["geo:Z"]=val.split(' ')
                #get station owner in siteOwner subsection
                elif own_flag:
                    if var=="gml:name":
                        metadata["geo:ownerAgency"]=val
                    if var=="gco:CharacterString":
                        if not metadata["geo:ownerContact"]:
                            metadata["geo:ownerContact"]=val
                #get station contact in siteContact subsection
                elif sit_flag:
                    if var=="gml:name":
                        metadata["geo:onSiteAgency"]=val
                    if var=="gco:CharacterString":
                        if not metadata["geo:onSiteContact"]:
                            metadata["geo:onSiteContact"]=val
                #retrieve metadata listed in metadata dictionnary keys (geo:...)
                elif var in metadata.keys():
                    if not metadata[var]:
                        metadata[var]=val

    return metadata

def rinex_insert(run_by,marker_name,marker_number,observer,agency,rec_sn,rec_type,rec_fw,ant_sn,ant_type,x,y,z,delta_h,delta_e,delta_n,OverwriteReceiver,OverwriteAntenna,KeepXYZ,KeepEcc,KeepMN,SatStat,Comment,RemoveHistory,Quiet,rnx_dict):
    #####################################
    # Insert metadata into Rinex header #
    #####################################
    new_header=''
    gsf_flag=True
    obs_flag=True
    interval_flag=False
    rec_flag=False
    rec_str=''
    ant_flag=False
    ant_str=''
    number_flag=False
    #loop for each line until end of header
    for line in rnx_dict["content"][:rnx_dict["header_index"]].split(rnx_dict["nl"]):
        if line[60:79] == "PGM / RUN BY / DATE":
            if run_by:
                new_header+=line[:20]+run_by[:20].ljust(20,' ')+line[40:80]+rnx_dict["nl"]
                if not RemoveHistory  and line[:60] != line[:20]+run_by[:20].ljust(20,' '):
                    new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
            else:
                new_header+=line[:80]+rnx_dict["nl"]
            #show gml2rnx.py information
            if rnx_dict["decimate"]:
                new_header+="gml2rnx.py v"+VERSION+"   MERGE / DECIMATION  "+datetime.utcnow().strftime("%Y%m%d %H%M%S UTC ")+"COMMENT"+rnx_dict["nl"]
            else:
                new_header+="gml2rnx.py v"+VERSION+"   HEADER MODIFICATION "+datetime.utcnow().strftime("%Y%m%d %H%M%S UTC ")+"COMMENT"+rnx_dict["nl"]
            if not RemoveHistory:
                new_header+="Original line is inserted in comment after modified line    COMMENT"+rnx_dict["nl"]
            if Comment:
                for c in Comment:
                    new_header+=c[:60].ljust(60,' ')+'COMMENT'+rnx_dict["nl"]
        elif line[60:71] == "MARKER NAME" and marker_name and not KeepMN:
            if rnx_dict["version"]<3.0:
                new_header+=marker_name[:4].ljust(60,' ')+line[60:80]+rnx_dict["nl"]
                if not RemoveHistory and line[:60] != marker_name[:4].ljust(60,' '):
                    new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
            else:
                new_header+=marker_name[:60].ljust(60,' ')+line[60:80]+rnx_dict["nl"]
                if not RemoveHistory and line[:60] != marker_name[:60].ljust(60,' '):
                    new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
        elif line[60:73] == "MARKER NUMBER" and marker_number:
            number_flag=True
            new_header+=marker_number[:60].ljust(60,' ')+line[60:80]+rnx_dict["nl"]
            if not RemoveHistory and line[:60] != marker_number[:60].ljust(60,' '):
                new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
        elif line[60:77] == "OBSERVER / AGENCY":
            if observer:
                new_header+=observer[:20].ljust(20,' ')
            else:
                new_header+=line[:20]
                observer=line[:20]
            if agency:
                new_header+=agency[:40].ljust(40,' ')+line[60:80]+rnx_dict["nl"]
            else:
                new_header+=line[20:80]+rnx_dict["nl"]
                agency=line[20:60]
            if not RemoveHistory and line[:60] != observer[:20].ljust(20,' ')+agency[:40].ljust(40,' '):
                new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
        elif line[60:79] == "REC # / TYPE / VERS":
            if rec_sn:
                rec_sn=rec_sn[:20].ljust(20,' ')
                rec_str+=rec_sn
                if rec_sn != line[:20]: rec_flag=True
            else:
                rec_str+=line[:20]
            if rec_type:
                rec_type=rec_type[:20].ljust(20,' ')
                rec_str+=rec_type
                if rec_type != line[20:40]: rec_flag=True
            else:
                rec_str+=line[20:40]
            if rec_fw:
                rec_fw=rec_fw[:20].ljust(20,' ')
                rec_str+=rec_fw+line[60:80]+rnx_dict["nl"]
                if rec_fw != line[40:60]: rec_flag=True
            else:
                rec_str+=line[40:80]+rnx_dict["nl"]
            if rec_flag:
                if not Quiet: print("WARNING: RECEIVER METADATA INCONSCIENCY:\nGML:",rec_sn+rec_type+rec_fw+line[60:80],"\nRNX:",line)
                if OverwriteReceiver:
                    new_header+=rec_str
                    if not RemoveHistory: new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
                else:
                    if not Quiet: print("Add --OverwriteReceiver option to store GeodesyML receiver information in RINEX")
                    new_header+=line+rnx_dict["nl"]
            else:
                new_header+=line+rnx_dict["nl"]
        elif line[60:72] == "ANT # / TYPE":
            if ant_sn:
                ant_sn=ant_sn[:20].ljust(20,' ')
                ant_str+=ant_sn
                if ant_sn != line[:20]: ant_flag=True
            else:
                ant_str+=line[:20]
            if ant_type:
                ant_type=ant_type[:40].ljust(40,' ')
                ant_str+=ant_type+line[60:80]+rnx_dict["nl"]
                if ant_type != line[20:60]: ant_flag=True
            else:
                ant_str+=line[20:80]+rnx_dict["nl"]
            if ant_flag:
                if not Quiet: print("WARNING: ANTENNA METADATA INCONSCIENCY:\nGML:",ant_sn+ant_type+line[60:80],"\nRNX:",line)
                if OverwriteAntenna:
                    new_header+=ant_str
                    if not RemoveHistory: new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
                else:
                    if not Quiet: print("Add --OverwriteAntenna option to store GeodesyML antenna information in RINEX")
                    new_header+=line+rnx_dict["nl"]
            else:
                new_header+=line+rnx_dict["nl"]
        elif line[60:79] == "APPROX POSITION XYZ" and not KeepXYZ:
            if x:
                new_header+='{:14.4f}'.format(float(x))
            else:
                new_header+=line[:14]
                x=float(line[:14])
            if y:
                new_header+='{:14.4f}'.format(float(y))
            else:
                new_header+=line[14:28]
                y=float(line[14:28])
            if z:
                new_header+='{:14.4f}{}{}'.format(float(z),line[42:80],rnx_dict["nl"])
            else:
                new_header+=line[28:80]+rnx_dict["nl"]
                z=float(line[28:42])
            if not RemoveHistory and line[:42] != '{:14.4f}'.format(float(x))+'{:14.4f}'.format(float(y))+'{:14.4f}'.format(float(z)):
                new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
        elif line[60:80] == "ANTENNA: DELTA H/E/N" and not KeepEcc:
            if delta_h:
                new_header+='{:14.4f}'.format(float(delta_h))
            else:
                new_header+=line[:14]
                delta_h=float(line[:14])
            if delta_e:
                new_header+='{:14.4f}'.format(float(delta_e))
            else:
                new_header+=line[14:28]
                delta_e=float(line[14:28])
            if delta_n:
                new_header+='{:14.4f}{}{}'.format(float(delta_n),line[42:80],rnx_dict["nl"])
            else:
                new_header+=line[28:80]+rnx_dict["nl"]
                delta_n=float(line[28:42])
            if not RemoveHistory and line[:42] != '{:14.4f}'.format(float(delta_h))+'{:14.4f}'.format(float(delta_e))+'{:14.4f}'.format(float(delta_n)):
                new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
        elif line[60:77] == "TIME OF FIRST OBS" and rnx_dict["decimate"]:
            new_header+=rnx_dict["first_obs"].strftime("  %Y    %m    %d    %H    %M   %S.0000000     GPS         TIME OF FIRST OBS").replace('   0','    ')+rnx_dict["nl"]
            if not RemoveHistory and line[:60] != rnx_dict["first_obs"].strftime("  %Y    %m    %d    %H    %M   %S.0000000     GPS         ").replace('   0','    '):
                new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
            new_header+=rnx_dict["last_obs"].strftime("  %Y    %m    %d    %H    %M   %S.0000000     GPS         TIME OF LAST OBS").replace('   0','    ')+rnx_dict["nl"]
        elif line[60:76] == "TIME OF LAST OBS" and rnx_dict["decimate"]:
            if not RemoveHistory and line[:60] != rnx_dict["last_obs"].strftime("  %Y    %m    %d    %H    %M   %S.0000000     GPS         ").replace('   0','    '):
                new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
        elif line[60:68] == "INTERVAL":
            interval_flag=True
            if rnx_dict["decimate"]:
                new_header+="    30.000                                                  INTERVAL"+rnx_dict["nl"]
                if not RemoveHistory:
                    new_header+=line[:60]+'COMMENT'+rnx_dict["nl"]
            else:
                new_header+=line+rnx_dict["nl"]
        elif line[60:75] == "# OF SATELLITES" and (rnx_dict["decimate"] or SatStat):
            pass
        elif line[60:74] == "PRN / # OF OBS" and (rnx_dict["decimate"] or SatStat):
            pass
        elif line[60:80] == "GLONASS SLOT / FRQ #" and rnx_dict["decimate"] and rnx_dict["gsf"]:
            #replace Glonass slot/frequencies entry with full list after decimation
            if gsf_flag:
                gsf_flag=False
                i=0
                #write the number of glonass slot/frequencies first
                new_header+=" {: >2} ".format(len(rnx_dict["gsf"]))
                for R in rnx_dict["gsf"].keys():
                    new_header+='R'+R+rnx_dict["gsf"][R]
                    i+=1
                    #add new line after 8 entry
                    if i==8: 
                        i=0
                        #no place left, add end of line
                        new_header+="GLONASS SLOT / FRQ #"+rnx_dict["nl"]
                        #add the 4 first spaces in the new line if not the last entry
                        if R != list(rnx_dict["gsf"].keys())[-1]:
                           new_header+="    "
                #add space padding and end of line
                if i!=0:
                    for i in range(i,8):
                       new_header+="       "
                    new_header+="GLONASS SLOT / FRQ #"+rnx_dict["nl"]
        elif line[60:79] == "# / TYPES OF OBSERV" and rnx_dict["decimate"]:
            #Rinex2
            #replace observation types list entry with full list after decimation
            if obs_flag:
                obs_flag=False
                i=0
                #write the number of observation type first
                new_header+="    {:>2}".format(len(rnx_dict["obs_types"]['M']))
                for T in rnx_dict["obs_types"]['M']:
                    new_header+="    "+T
                    i+=1
                    #add new line after 9 entry
                    if i==9: 
                        i=0
                        #no place left, add end of line
                        new_header+="# / TYPES OF OBSERV"+rnx_dict["nl"]
                        #add the 6 first spaces in the new line if not the last entry
                        if T != rnx_dict["obs_types"]['M'][-1]:
                           new_header+="      "
                #add space padding and end of line
                if i!=0:
                    for i in range(i,9):
                       new_header+="      "
                    new_header+="# / TYPES OF OBSERV"+rnx_dict["nl"]
        elif line[60:79] == "SYS / # / OBS TYPES" and rnx_dict["decimate"]:
            #Rinex3
            #replace observation types list entry with full list after decimation
            if obs_flag:
                obs_flag=False
                #Constellation loop
                for C in ('G', 'R', 'E', 'S', 'C', 'J'):
                    #skip empty obs
                    if not rnx_dict["obs_types"][C]:
                        continue
                    i=0
                    #write constelation letter and number of observation type first
                    new_header+=C+"   {:>2}".format(len(rnx_dict["obs_types"][C]))
                    for T in rnx_dict["obs_types"][C]:
                        #new_header+="   "+T
                        new_header+=" "+T
                        i+=1
                        #add new line after 1333 entry
                        if i==13: 
                            i=0
                            #no place left, add end of line
                            new_header+="  SYS / # / OBS TYPES"+rnx_dict["nl"]
                            #add the 8 first spaces in the new line if not the last entry
                            if T != rnx_dict["obs_types"][C][-1]:
                               new_header+="      "
                    #add space padding and end of line
                    if i!=0:
                        for i in range(i,13):
                           new_header+="    "
                        new_header+="  SYS / # / OBS TYPES"+rnx_dict["nl"]
        elif line[60:73] == "END OF HEADER":
            #add interval if not present in the header
            if not interval_flag:
                new_header+="    {:02d}.000                                                  INTERVAL".format(rnx_dict["interval"])+rnx_dict["nl"]
            #add marker number if not present in the header
            if not number_flag and marker_number:
                new_header+=marker_number[:60].ljust(60,' ')+"MARKER NUMBER"+rnx_dict["nl"]
            #add satelite observation statistics before end of header
            if SatStat:
                new_header+=str(len(rnx_dict["obs_sat"])).rjust(6)+"                                                      # OF SATELLITES"+rnx_dict["nl"]
                #loop for each satelite
                for sat in sorted(rnx_dict["obs_sat"].keys()):
                    #print PRN
                    new_header+=sat.rjust(6)
                    j=0
                    #loop for each type of observation
                    for i in range(len(rnx_dict["obs_sat"][sat])):
                        if j==-1:
                            #add space padding on new line
                            new_header+="      "
                            j=0
                        new_header+=str(rnx_dict["obs_sat"][sat][i]).rjust(6)
                        j+=1
                        if j==9:
                            #add end of line after 9 type of obs
                            new_header+="PRN / # OF OBS"+rnx_dict["nl"]
                            j=-1
                    #case of line not finnished
                    if j!=-1:
                        #add spaces and final end of line
                        for i in range(j,9):
                            new_header+="      "
                        new_header+="PRN / # OF OBS"+rnx_dict["nl"]
                #add observation statistics in comment
                new_header+="NB OBS: {} / {} = {} %".format(rnx_dict["nb_obs"],rnx_dict["nb_obs_typ"],round(rnx_dict["nb_obs"]/rnx_dict["nb_obs_typ"]*100)).ljust(60,' ')+"COMMENT"+rnx_dict["nl"]
            new_header+=line
        else:
            new_header+=line+rnx_dict["nl"]

    #concatenate new header and obsevations
    rnx_dict["content"]=new_header+rnx_dict["content"][rnx_dict["header_index"]:]
    #set the new header index
    rnx_dict["header_index"]=len(new_header)
    #rename rinex filename to 30s/24h after decimation
    if rnx_dict["decimate"]:
        rnx_dict["duration"]="01D"
        if rnx_dict["type"][3]=='2':
            #rinex2
            rnx_dict["bname"]=rnx_dict["bname"][:7]+'0'+rnx_dict["bname"][8:]
            rnx_dict["name"]=rnx_dict["name"][:-len(rnx_dict["bname"])]+rnx_dict["bname"]
        else:
            #rinex3
            rnx_dict["bname"]=rnx_dict["bname"][:24]+"01D_30S"+rnx_dict["bname"][31:]
            rnx_dict["name"]=rnx_dict["name"][:-len(rnx_dict["bname"])]+rnx_dict["bname"]

    return rnx_dict

def write_output(Overwrite,OutDirectory,OutType,Checksum,rnx_dict):
    ########################
    # write new rinex file #
    ########################
    #convert rinex content string to binary
    if isinstance(rnx_dict["content"], str): 
        rnx_dict["content"]=rnx_dict["content"].encode('utf-8','replace')
    #special case: rinex3 with rinex2 filename convention
    if rnx_dict["type"][3]=='2'and rnx_dict["version"]>=3.0:
        rnx_dict["type"]=rnx_dict["type"].replace('2','3')
        OutName=rnx_dict["site"]+rnx_dict["_R_"]+rnx_dict["date"].strftime("%Y%j%H%M_")+rnx_dict["duration"]+"_{:02d}S".format(rnx_dict["interval"])+"_MO.rnx"
        if rnx_dict["type"][2]=='z':
            OutName=OutName+".gz"
        if rnx_dict["type"][0]=='c':
            OutName=OutName.replace(".rnx",".crx")
    #use the same rinex filename and compression by default
    else:
        OutName=path.basename(rnx_dict["name"])
    if not OutType:
        OutType=rnx_dict["type"]
    #initilise checksum
    rnx_md5=''
    crnx_md5=''
    #compute rinex md5 checksum if content is already uncompressed 
    if Checksum and (rnx_dict["type"][0] == 'r' or rnx_dict["hatanaka_decompress"]):
        rnx_md5=md5(rnx_dict["content"]).hexdigest()
    #uncompress rinex and compute md5 checksum if content is compressed and output is also compressed
    elif Checksum and (rnx_dict["type"][0] == 'c' and OutType[0] == 'c'):
        #hatanaka deflate content
        if rnx2crx=='hatanaka lib':
            rnx_md5=md5(hatanaka.crx2rnx(rnx_dict["content"])).hexdigest()
        elif crx2rnx:
            crinex = Popen([crx2rnx, '-'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (crinex_out, crinex_err) = crinex.communicate(input=rnx_dict["content"])
            rnx_md5=md5(crinex_out).hexdigest()
        else:
            print('ERROR: Cannot perform hatanaka decompression on rinex file. Please install hatanaka library (ie. pip3 install hatanaka) or RNXCMP software on your system.', file=stderr, flush=True)
            exit()
    #convert content and filename with hatanaka
    if rnx_dict["type"][0] == 'c' and OutType[0] == 'r':
        #remove hatanaka compression filename
        if rnx_dict["type"][3] == '2':
            #Rinex2
            OutName=OutName[:11]+'o'+OutName[12:]
        else:
            #Rinex3
            OutName=OutName.replace('.crx','.rnx')
        #content already deflated
        if not rnx_dict["hatanaka_decompress"]:
            #hatanaka deflate content
            if rnx2crx=='hatanaka lib':
                rnx_dict["content"]=hatanaka.crx2rnx(rnx_dict["content"])
            elif crx2rnx:
                crinex = Popen([crx2rnx, '-'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
                (rnx_dict["content"], crinex_err) = crinex.communicate(input=rnx_dict["content"])
            else:
                print('ERROR: Cannot perform hatanaka decompression on rinex file. Please install hatanaka library (ie. pip3 install hatanaka) or RNXCMP software on your system.', file=stderr, flush=True)
                exit()
            #compute rinex md5 checksum on previously uncompressed content
            if Checksum:
                rnx_md5=md5(rnx_dict["content"]).hexdigest()
    elif (rnx_dict["type"][0] == 'r' or rnx_dict["hatanaka_decompress"]) and OutType[0] == 'c':
        #use hatanaka compression filename
        if rnx_dict["type"][3] == '2':
            #Rinex2
            OutName=OutName[:11]+'d'+OutName[12:]
        else:
            #Rinex3
            OutName=OutName.replace('.rnx','.crx')
        if Checksum:
            rnx_md5=md5(rnx_dict["content"]).hexdigest()
        #hatanaka compress content
        if rnx2crx=='hatanaka lib':
            rnx_dict["content"]=hatanaka.rnx2crx(rnx_dict["content"])
        elif rnx2crx:
            crinex = Popen([rnx2crx, '-'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (rnx_dict["content"], crinex_err) = crinex.communicate(input=rnx_dict["content"])
        else:
            print('ERROR: Cannot perform hatanaka compression on rinex file. Please install hatanaka library (ie. pip3 install hatanaka) or RNXCMP software on your system.', file=stderr, flush=True)
            exit()
    #rename filename with Gzip
    if rnx_dict["type"][2] == 'z' and OutType[2] == 'x':
        #remove Gzip compression filename
        OutName=OutName[:-3]+OutName[-3:].replace('.Z','').replace('.z','').replace('.gz','')
    elif rnx_dict["type"][2] == 'x' and OutType[2] == 'z':
        #use Gzip compression filename
        if rnx_dict["type"][3] == '2':
            #Rinex2
            OutName+='.Z'
        else:
            #Rinex3
            OutName+='.gz'
    #compress content with Gzip
    if OutType[2] == 'z':
        if pgzip_BS:
            rnx_dict["content"]=pgzip.compress(rnx_dict["content"],blocksize=pgzip_BS)
        else:
            rnx_dict["content"]=gzip.compress(rnx_dict["content"])
    #compute rinex md5 checksum on previously or already compressed content
    if Checksum and (OutType[0] == 'c' or OutType[2] == 'z'):
        crnx_md5=md5(rnx_dict["content"]).hexdigest()

    #rename filename with forced metadata sitename
    if rnx_dict["new_site"]:
        if len(rnx_dict["new_site"])==4:
            OutName=rnx_dict["new_site"]+OutName[4:]
        elif len(rnx_dict["new_site"])==9 and rnx_dict["type"][3]=='3':
            OutName=rnx_dict["new_site"]+OutName[9:]
        elif len(rnx_dict["new_site"])==9 and rnx_dict["type"][3]=='2':
            OutName=rnx_dict["new_site"][:4]+OutName[4:]
        else:
            print('ERROR: Sitename specified into forced metadata option have to be written in 4 or 9 character (ie. SOPH or SOPH00FRA).', file=stderr, flush=True)
            exit()
    
    #no specified output directory 
    if not OutDirectory:
        #write into source directory
        OutDirectory=path.dirname(rnx_dict["name"])
    else:
        #POSIX date format in output directory string
        if '%' in OutDirectory:
            #format path with rinex date
            OutDirectory=rnx_dict["date"].strftime(OutDirectory)        
        # '#' special key in output directory string
        if '#' in OutDirectory:
            #format path with rinex inteval and duration info
            OutDirectory=OutDirectory.replace("#INTERVAL","{:02d}S".format(rnx_dict["interval"]))       
            OutDirectory=OutDirectory.replace("#interval","{:d}s".format(rnx_dict["interval"]))       
            OutDirectory=OutDirectory.replace("#DURATION",rnx_dict["duration"])       
            OutDirectory=OutDirectory.replace("#duration","{:d}{}".format(int(rnx_dict["duration"][:2]),rnx_dict["duration"][2].lower()))       
    #the specified directory doesn't exist
    if OutDirectory and not path.isdir(OutDirectory):
        #create new output directory
        makedirs(OutDirectory, exist_ok=True)
    #write new file into specified directory
    outfile=path.join(OutDirectory,OutName)
    #check overwritting
    if not Overwrite and path.isfile(outfile):
        outfile+='.new'
        if not args.Quiet: print('NOTICE: Output file already exist. Please add -f to ovewrite.\nNOTICE: To prevent overwriting, the output file will be: '+outfile, file=stderr, flush=True)
    open(outfile,'wb').write(rnx_dict["content"])

    #add md5 checksums comma separated after outfile path
    if Checksum:
        outfile=','.join([outfile,rnx_md5,crnx_md5])
    return outfile

def decimate(rnx_dict,satstat):
    #hatanaka deflate content
    if rnx_dict["type"][0] == 'c':
        if rnx2crx=='hatanaka lib':
            rnx_dict["content"]=hatanaka.crx2rnx(rnx_dict["content"])
        elif crx2rnx:
            crinex = Popen([crx2rnx, '-'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (rnx_dict["content"], crinex_err) = crinex.communicate(input=rnx_dict["content"].encode('utf-8','replace'))
            rnx_dict["content"]=rnx_dict["content"].decode('utf-8','replace')
        else:
            print('ERROR: Cannot perform hatanaka decompression on rinex file. Please install hatanaka library (ie. pip3 install hatanaka) or RNXCMP software on your system.', file=stderr, flush=True)
            exit()
        if rnx_dict["nl"]=='\n':
            #remove the 2 first CRINEX lines from header index
            rnx_dict["header_index"]-=80+78+2*len(rnx_dict["nl"])
        else:
            #after hatanaka decompressing, file will be in unix text type
            rnx_dict["nl"]='\n'
            #get rinex end of header string index
            rnx_dict["header_index"]=rnx_dict["content"].find("END OF HEADER",0,25000)
            #relocate header index just after new line
            rnx_dict["header_index"]=rnx_dict["content"].find(rnx_dict["nl"],rnx_dict["header_index"],rnx_dict["header_index"]+22)+len(rnx_dict["nl"])

    #configure observation date frame
    if rnx_dict["type"][3] == '2':
       #rinex2 obserservation frame
        rnx_dict["date_frame"]=rnx_dict["date"].strftime(" %y %m %d ")
    else:
        #rinex3 obserservation frame
        rnx_dict["date_frame"]=rnx_dict["date"].strftime("> %Y %m %d ")
    #date frame space padded
    rnx_dict["date_frame_sp"]=rnx_dict["date_frame"].replace(' 0','  ')

    #update Glonass slot frequencies
    gsf_str=''
    if "gsf" not in rnx_dict.keys():
        rnx_dict["gsf"]=dict()
    for line in rnx_dict["content"][:rnx_dict["header_index"]].split(rnx_dict["nl"]):
        if line[60:80] == "GLONASS SLOT / FRQ #":
            gsf_str+=line[:60]
    for R in gsf_str.split('R')[1:]:
        rnx_dict["gsf"][R[:2]]=R[2:6]

    #get observation type
    rnx_dict["obs_types"]={ 'M' : [], 'G' : [], 'R' : [], 'E' : [], 'S' : [], 'C' : [], 'J' : [], 'I' : []}
    if rnx_dict["type"][3] == '2':
        for line in rnx_dict["content"][:rnx_dict["header_index"]].split(rnx_dict["nl"]):
            if line[60:79] == "# / TYPES OF OBSERV":
                rnx_dict["obs_types"]['M']+=line[6:60].split()
    else:
        for line in rnx_dict["content"][:rnx_dict["header_index"]].split(rnx_dict["nl"]):
            if line[60:79] == "SYS / # / OBS TYPES":
                if line[0] != ' ':
                    cur_sat=line[0]
                rnx_dict["obs_types"][cur_sat]+=line[7:60].split()

    #decimate observations
    o_flag=False
    obs=''
    rnx_dict["first_obs"]=None
    nb_sat=0
    rnx_dict["obs_sat"]=dict()
    j=0
    sat_id=0
    #loop for each observation lines
    for line in rnx_dict["content"][rnx_dict["header_index"]:].split(rnx_dict["nl"]):
        #store decimate observations
        if line[:len(rnx_dict["date_frame"])] in (rnx_dict["date_frame"],rnx_dict["date_frame_sp"]):
            if int(line[len(rnx_dict["date_frame"])+6:len(rnx_dict["date_frame"])+8]) % 30 == 0:
                o_flag=True
                obs+=line+rnx_dict["nl"]
                #rinex2
                if rnx_dict["version"]<3.0:
                    #store current observation datetime as last observation
                    rnx_dict["last_obs"]=datetime.strptime(line[:18]," %y %m %d %H %M %S")
                    if satstat:
                        #store observed satellites
                        nb_sat=int(line[30:32])
                        cur_obs_sat=[]
                        sat_id=0
                        for i in range(32+3,len(line)+1,3):
                            sat=line[i-3:i]
                            cur_obs_sat.append(sat)
                            if sat not in rnx_dict["obs_sat"].keys():
                                rnx_dict["obs_sat"][sat]=[0]*len(rnx_dict["obs_types"]['M'])
                            nb_sat-=1
                #rinex3
                else:
                    #store current observation datetime as last observation
                    rnx_dict["last_obs"]=datetime.strptime(line[:21],"> %Y %m %d %H %M %S")
                #store current observation as first observation if not defined
                if not rnx_dict["first_obs"]:
                    rnx_dict["first_obs"]=rnx_dict["last_obs"]
            else:
                o_flag=False
        elif o_flag and line and line[0] in (' ','>'):
            if not args.Quiet: print("NOTICE: Observation out of range, skipping epoch:",line)
            o_flag=False
        elif o_flag and line:
            obs+=line+rnx_dict["nl"]
            #Compute satellite statistics
            if satstat:
                #rinex2
                if rnx_dict["version"]<3.0:
                    if nb_sat:
                        #store observed satellites (multiple head lines in Rinex2, 12sat/line)
                        for i in range(32+3,len(line)+1,3):
                            sat=line[i-3:i]
                            cur_obs_sat.append(sat)
                            if sat not in rnx_dict["obs_sat"].keys():
                                rnx_dict["obs_sat"][sat]=[0]*len(rnx_dict["obs_types"]['M'])
                            nb_sat-=1
                    else:
                        #read 5 observation by line
                        for i in range(5):
                            if line[i*16:i*16+16] and line[i*16:i*16+16] != "                ":
                                rnx_dict["obs_sat"][cur_obs_sat[sat_id]][j]+=1
                            #update next observation counter
                            j+=1
                        #all observation have been counted for current satelite, update next satelite counter
                        if j >= len(rnx_dict["obs_types"]['M']):
                            j=0
                            sat_id+=1
             
                #rinex3
                else:
                    #store observed satellites (head of each observation line)
                    sat=line[0:3]
                    if sat not in rnx_dict["obs_sat"].keys():
                        rnx_dict["obs_sat"][sat]=[0]*len(rnx_dict["obs_types"][sat[0]])
                    #read all observation
                    for i in range(len(rnx_dict["obs_sat"][sat])):
                            if line[i*16+3:i*16+19] and line[i*16+3:i*16+19] != "                ":
                                rnx_dict["obs_sat"][sat][i]+=1

    #overwrite content with header and decimated observations
    rnx_dict["content"]=rnx_dict["content"][:rnx_dict["header_index"]]+obs
    rnx_dict["decimate"]=True
    rnx_dict["hatanaka_decompress"]=True
    rnx_dict["interval"]=30
    return rnx_dict

def satstat(rnx_dict,out_type):
    #hatanaka deflate content
    if rnx_dict["type"][0] == 'c':
        #save hatanaka compressed content
        compressed_content=rnx_dict["content"]
        compressed_header_index=rnx_dict["header_index"]
        compressed_newline=rnx_dict["nl"]
        if rnx2crx=='hatanaka lib':
            rnx_dict["content"]=hatanaka.crx2rnx(rnx_dict["content"])
        elif crx2rnx:
            crinex = Popen([crx2rnx, '-'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
            rnx_dict["content"] = crinex.communicate(input=rnx_dict["content"].encode('utf-8','replace'))[0].decode('utf-8','replace')
        else:
            print('ERROR: Cannot perform hatanaka decompression on rinex file. Please install hatanaka library (ie. pip3 install hatanaka) or RNXCMP software on your system.', file=stderr, flush=True)
            exit()
        if rnx_dict["nl"]=='\n':
            #remove the 2 first CRINEX lines from header index
            rnx_dict["header_index"]-=80+78+2*len(rnx_dict["nl"])
        else:
            #after hatanaka decompressing, file will be in unix text type
            rnx_dict["nl"]='\n'
            #get rinex end of header string index
            rnx_dict["header_index"]=rnx_dict["content"].find("END OF HEADER",0,25000)
            #relocate header index just after new line
            rnx_dict["header_index"]=rnx_dict["content"].find(rnx_dict["nl"],rnx_dict["header_index"],rnx_dict["header_index"]+22)+len(rnx_dict["nl"])


    #configure observation date frame
    if rnx_dict["version"]<3.0:
       #rinex2 obserservation frame
        rnx_dict["date_frame"]=rnx_dict["date"].strftime(" %y %m %d ")
    else:
        #rinex3 obserservation frame
        rnx_dict["date_frame"]=rnx_dict["date"].strftime("> %Y %m %d ")
    #date frame space padded
    rnx_dict["date_frame_sp"]=rnx_dict["date_frame"].replace(' 0','  ')

    #get list of observation type from header
    rnx_dict["obs_types"]={ 'M' : [], 'G' : [], 'R' : [], 'E' : [], 'S' : [], 'C' : [], 'J' : [], 'I' : []}
    if rnx_dict["version"]<3.0:
        for line in rnx_dict["content"][:rnx_dict["header_index"]].split(rnx_dict["nl"]):
            if line[60:79] == "# / TYPES OF OBSERV":
                rnx_dict["obs_types"]['M']+=line[6:60].split()
    else:
        for line in rnx_dict["content"][:rnx_dict["header_index"]].split(rnx_dict["nl"]):
            if line[60:79] == "SYS / # / OBS TYPES":
                if line[0] != ' ':
                    cur_sat=line[0]
                rnx_dict["obs_types"][cur_sat]+=line[7:60].split()
    #get observations statistics
    nb_sat=0
    rnx_dict["obs_sat"]=dict()
    j=0
    sat_id=0
    rnx_dict["nb_obs"]=0
    #loop for each observation lines
    for line in rnx_dict["content"][rnx_dict["header_index"]:].split(rnx_dict["nl"]):
        #head of observations
        if line[:len(rnx_dict["date_frame"])] in (rnx_dict["date_frame"],rnx_dict["date_frame_sp"]):
            rnx_dict["nb_obs"]+=1
            #rinex2
            if rnx_dict["version"]<3.0:
                #store observed satellites
                nb_sat=int(line[30:32])
                cur_obs_sat=[]
                sat_id=0
                for i in range(32+3,len(line)+1,3):
                    sat=line[i-3:i]
                    cur_obs_sat.append(sat)
                    if sat not in rnx_dict["obs_sat"].keys():
                        rnx_dict["obs_sat"][sat]=[0]*len(rnx_dict["obs_types"]['M'])
                    nb_sat-=1
        #observation data
        else:
            #rinex2
            if rnx_dict["version"]<3.0:
                if nb_sat:
                    #store observed satellites (multiple head lines in Rinex2, 12sat/line)
                    for i in range(32+3,len(line)+1,3):
                        sat=line[i-3:i]
                        cur_obs_sat.append(sat)
                        if sat not in rnx_dict["obs_sat"].keys():
                            rnx_dict["obs_sat"][sat]=[0]*len(rnx_dict["obs_types"]['M'])
                        nb_sat-=1
                else:
                    #read 5 observation by line
                    for i in range(5):
                        if line[i*16:i*16+16] and line[i*16:i*16+16] != "                ":
                            rnx_dict["obs_sat"][cur_obs_sat[sat_id]][j]+=1
                        #update next observation counter
                        j+=1
                    #all observation have been counted for current satelite, update next satelite counter
                    if j >= len(rnx_dict["obs_types"]['M']):
                        j=0
                        sat_id+=1
            #rinex3
            elif line:
                #store observed satellites (head of each observation line)
                sat=line[0:3]
                if sat not in rnx_dict["obs_sat"].keys():
                    rnx_dict["obs_sat"][sat]=[0]*len(rnx_dict["obs_types"][sat[0]])
                #read all observation
                for i in range(len(rnx_dict["obs_sat"][sat])):
                        if line[i*16+3:i*16+19] and line[i*16+3:i*16+19] != "                ":
                            rnx_dict["obs_sat"][sat][i]+=1
    #calculate typical number of observations
    if rnx_dict["duration"] == "01D" : rnx_dict["nb_obs_typ"]=int((86400-(rnx_dict["date"]-rnx_dict["date"].replace(hour=0, minute=0,second=0)).seconds)/rnx_dict["interval"])
    elif rnx_dict["duration"] == "01H" : rnx_dict["nb_obs_typ"]=int((3600-(rnx_dict["date"]-rnx_dict["date"].replace(minute=0,second=0)).seconds)/rnx_dict["interval"])

    if rnx_dict["type"][0] == 'c':
        if out_type is None or out_type[0] == 'c':
            #restore hatanaka compressed content
            rnx_dict["content"]=compressed_content
            rnx_dict["header_index"]=compressed_header_index
            rnx_dict["nl"]=compressed_newline
        else:
            rnx_dict["hatanaka_decompress"]=True

    return rnx_dict

def printsatstat(rnx_dict):
    #print observation statistics
    print("NB OBS:",rnx_dict["nb_obs"],'/',rnx_dict["nb_obs_typ"],'=',round(rnx_dict["nb_obs"]/rnx_dict["nb_obs_typ"]*100),'%\n')
    #print satelite statitics
    #rinex2
    if rnx_dict["version"]<3.0:
        print("PRN,  "+",  ".join(rnx_dict["obs_types"]['M']))
        for sat in sorted(rnx_dict["obs_sat"].keys()):
            print(sat+','+','.join(map(lambda x: format(x,"4") ,rnx_dict["obs_sat"][sat])))
    #rinex3
    else:
        for C in rnx_dict["obs_types"].keys():
            if rnx_dict["obs_types"][C]:
                print("PRN, "+", ".join(rnx_dict["obs_types"][C]))
                for sat in sorted(rnx_dict["obs_sat"].keys()):
                    if sat[0] == C:
                        print(sat+','+','.join(map(lambda x: format(x,"4") ,rnx_dict["obs_sat"][sat])))
                print('\n')


def merge(rnx_dict,obs_type):
    #Arrange observations values according to new observation type list
    content=''
    #loop for each observation lines
    for line in rnx_dict["content"][rnx_dict["header_index"]:].split(rnx_dict["nl"]):
        #head of observations
        if line[:len(rnx_dict["date_frame"])] in (rnx_dict["date_frame"],rnx_dict["date_frame_sp"]):
            #insert head of observations
            content+=line+rnx_dict["nl"]
            #rinex2
            if rnx_dict["version"]<3.0:
                #store number of observed satellites
                nb_sat=int(line[30:32])
                nb_sat_header_read=int((len(line)-32)/3)
                obs=dict()
                obs_id=0
        #observation data
        else:
            #rinex2
            if rnx_dict["version"]<3.0:
                #read next satelites list line
                if nb_sat_header_read<nb_sat:
                    content+=line+rnx_dict["nl"]
                    nb_sat_header_read+=(len(line)-32)/3
                #store observation
                else:
                    #read 5 observation by line
                    for i in range(5):
                        #check latest obs
                        if obs_id < len(rnx_dict["obs_types"]['M']):
                            if line[i*16:i*16+16]:
                                obs[rnx_dict["obs_types"]['M'][obs_id]]=line[i*16:i*16+16]
                            else:
                                obs[rnx_dict["obs_types"]['M'][obs_id]]="                "
                            #update next observation counter
                            obs_id+=1
                    #generate new satelite observation
                    if obs_id >= len(rnx_dict["obs_types"]['M']):
                        #reset next observation counter
                        obs_id=0
                        #generate new observation
                        for i in range(len(obs_type['M'])):
                            #new observation already in rinex
                            if obs_type['M'][i] in rnx_dict["obs_types"]['M']:
                                content+=obs[obs_type['M'][i]]+' '*(16-len(obs[obs_type['M'][i]]))
                            #insert empty obs
                            else:
                                content+="                "
                            #new line every 5 obs except if last obs
                            if (i+1)%5==0 and i!=len(obs_type['M'])-1:
                                content+=rnx_dict["nl"]
                        #insert final new line
                        content+=rnx_dict["nl"]
            #rinex3
            elif line:
                #store and insert observed satellites
                sat=line[0:3]
                content+=sat
                #loop for all new observation type list
                for T in obs_type[sat[0]]:
                    try:
                        #find and insert observation value
                        i=rnx_dict["obs_types"][sat[0]].index(T)
                        content+=line[i*16+3:i*16+19]
                    except ValueError:
                        #insert empty obs
                        content+="                "
                #insert final new line
                content+=rnx_dict["nl"]
    #update satellite statistics
    if "obs_sat" in rnx_dict.keys():
        merged_obs_sat=dict()
        for sat in rnx_dict["obs_sat"].keys():
            merged_obs_sat[sat]=[]
            for k in obs_type :
                if k=='M' or sat[0]==k:
                    i=0
                    for t in range(len(obs_type[k])):
                        if obs_type[k][t] in rnx_dict["obs_types"][k]:
                            merged_obs_sat[sat].append(rnx_dict["obs_sat"][sat][i])
                            i+=1
                        else:
                            merged_obs_sat[sat].append(0)
    rnx_dict["obs_sat"]=merged_obs_sat
    #update observation type list
    rnx_dict["obs_types"]=obs_type
    #overwrite content
    rnx_dict["content"]=rnx_dict["content"][:rnx_dict["header_index"]]+content
    return rnx_dict
                
def concatenate(rnx_dict,rnx_dict_new):
    #no epoch in old rinex
    if "last_obs" not in rnx_dict.keys():
        if not args.Quiet: print("NOTICE: Old rinex contain no data, Skip concatenation...")
        #skip rinex concatenation
        return rnx_dict_new
    #no new epoch in new rinex
    if "last_obs" not in rnx_dict_new.keys() or rnx_dict["last_obs"] >= rnx_dict_new["last_obs"]:
        if not args.Quiet: print("NOTICE: New rinex contain less epoch than previous, Skip concatenation...")
        #skip rinex concatenation
        return rnx_dict
    #epoch ovelap
    if rnx_dict["last_obs"] >= rnx_dict_new["first_obs"]:
        if not args.Quiet: print("NOTICE: Epoch overlap. Previous rinex end at",rnx_dict["last_obs"],",new rinex start at",rnx_dict_new["first_obs"])
        #erase ovelap epoch in new rinex content
        store_flag=False
        #initiate new content whith header
        content=rnx_dict_new["content"][:rnx_dict_new["header_index"]]
        #parse each rinex observation line
        for line in rnx_dict_new["content"][rnx_dict_new["header_index"]:].split(rnx_dict_new["nl"]):
            #get the head of observations line
            if line[:len(rnx_dict_new["date_frame"])] in (rnx_dict_new["date_frame"],rnx_dict_new["date_frame_sp"]):
                #rinex2
                if rnx_dict["version"]<3.0:
                    #compare current new rinex observation datetime and rinex last observation
                    if rnx_dict["last_obs"] < datetime.strptime(line[:18]," %y %m %d %H %M %S"):
                        store_flag=True
                #rinex3
                else:
                    #compare current new rinex observation datetime and rinex last observation
                    if rnx_dict["last_obs"] < datetime.strptime(line[:21],"> %Y %m %d %H %M %S"):
                        store_flag=True
            if store_flag:
                #store new content
                content+=line+rnx_dict_new["nl"]
        rnx_dict_new["content"]=content[:-len(rnx_dict_new["nl"])]
    #observations mismatch
    if rnx_dict["obs_types"] != rnx_dict_new["obs_types"]:
        #merge observation type list (respect the order)
        merged_obs_type={ 'M' : [], 'G' : [], 'R' : [], 'E' : [], 'S' : [], 'C' : [], 'J' : []}
        #loop for all satellite constellation
        for C in merged_obs_type.keys():
            #no observation for curent constellation in previous rinex
            if len(rnx_dict["obs_types"][C]) == 0:
                merged_obs_type[C]=rnx_dict_new["obs_types"][C]
                continue
            #no observation for curent constellation in new rinex
            if len(rnx_dict_new["obs_types"][C]) == 0:
                merged_obs_type[C]=rnx_dict["obs_types"][C]
                continue
            i=j=0
            #loop for all observatins type
            while True:
                if i == len(rnx_dict["obs_types"][C]):
                    #end of previous obs type
                    i=-1
                if j == len(rnx_dict_new["obs_types"][C]):
                    #end of new obs type
                    j=-1
                if i==-1 and j==-1:
                    #end of previous and new obs type, beak the loop
                    break
                if rnx_dict["obs_types"][C][i] == rnx_dict_new["obs_types"][C][j]:
                    #previous and new rinex obs type are the same
                    merged_obs_type[C].append(rnx_dict["obs_types"][C][i])
                    i+=1
                    j+=1
                elif rnx_dict["obs_types"][C][i] != rnx_dict_new["obs_types"][C][j] and rnx_dict["obs_types"][C][i] not in rnx_dict_new["obs_types"][C]:
                    #new obs type in previous rinex
                    merged_obs_type[C].append(rnx_dict["obs_types"][C][i])
                    i+=1
                elif rnx_dict["obs_types"][C][i] != rnx_dict_new["obs_types"][C][j] and rnx_dict_new["obs_types"][C][j] not in rnx_dict["obs_types"][C]:
                    #new obs type in new rinex
                    merged_obs_type[C].append(rnx_dict_new["obs_types"][C][j])
                    j+=1
                else:
                    #new obs type in previous and new rinex
                    merged_obs_type[C].append(rnx_dict["obs_types"][C][i])
                    merged_obs_type[C].append(rnx_dict_new["obs_types"][C][j])
                    i+=1
                    j+=1
        #arrange previous rinex
        if rnx_dict["obs_types"] != merged_obs_type:
            if not args.Quiet: print("NOTICE: Observation types mismatch. Merge previous rinex obsvervations.")
            rnx_dict=merge(rnx_dict,merged_obs_type)
        #arrange new rinex
        if rnx_dict_new["obs_types"] != merged_obs_type:
            if not args.Quiet: print("NOTICE: Observation types mismatch. Merge new rinex obsvervations.")
            rnx_dict_new=merge(rnx_dict_new,merged_obs_type)
    #append contents
    rnx_dict["content"]+=rnx_dict_new["content"][rnx_dict_new["header_index"]:]
    #update last obs datetime
    rnx_dict["last_obs"]=rnx_dict_new["last_obs"]
    #update Glonass slot frequencies
    rnx_dict["gsf"]={**rnx_dict["gsf"], **rnx_dict_new["gsf"]}
    #update satelites observation statistics
    for sat in rnx_dict_new["obs_sat"].keys():
        if sat not in rnx_dict["obs_sat"]:
            rnx_dict["obs_sat"][sat]=rnx_dict_new["obs_sat"][sat]
        else:
            for i in range(len(rnx_dict["obs_sat"][sat])):
                rnx_dict["obs_sat"][sat][i]+=rnx_dict_new["obs_sat"][sat][i]
    return rnx_dict

######## 
# Main #
########

#Check argument
parser = argparse.ArgumentParser(description='Functionalities:\n\t\t  Fill Rinex header from GLASS-API, M3G-API or GeodesyML metadata.\n\t\t  Default modified sections: RUN BY, MARKER NAME, MARKER NUMBER, OBSERVER / AGENCY, ANT # / TYPE, APPROX POSITION XYZ, ANTENNA: DELTA H/E/N.\n\t\t  Optionnal modified sections: REC # / TYPE / VERS, # OF SATELLITES, PRN / # OF OBS, COMMENT.\n\n\t\t  Decimate and concatenate multiple Rinex files to 30s daily files.\n\t\t  Rinex2 to Rinex3 conversion and vice versa is not supported.\n\n\t\t  View the observation summary contains in Rinex file, this can be inserted in Rinex header.\n\n\t\t  Download the latest GeodesyML file, this can be archived into repository architecture.', formatter_class=argparse.RawTextHelpFormatter, prog='gml2rnx.py')
parser.add_argument('--Version', '-v', action='version', version='%(prog)s v'+VERSION)
parser.add_argument('Rinex', metavar='Rinex_file', type=argparse.FileType('rb'), nargs='*',
                    help='Rinex2 or Rinex3 files to be processed.\nHatanaka and Gzip compression supported.\nFor huge files processing, please use --InputPath option.')
parser.add_argument('--InputPath', '-i', metavar='Rinex_path', type=str, default=None,
                    help='Input directory path to process Rinex files.\nWildcards is permited using quotes.\nie: ./gml2rnx.py -i "/data/2024/???/SITE00FRA*" ')
parser.add_argument('--Quiet', '-q', action='store_true', default=False,
                    help='Disable notifications.\nGenerated output files will be printed on stdout, errors on stderr.')
parser.add_argument('--GeodesyML', '-g', metavar='{DGW, M3G} or GeodesyML path', type=str, default='DGW',
                    help='GeodesyML path or repository to get metadata.\nBy default it will retrieve metadata from DGW GLASS-API.')
parser.add_argument('--GeodesyMLoutDirectory', '-gd', metavar='GeodesyML store path', type=str, default=None,
                    help='Output directory path to store GeodesyML.\nDate POSIX format accepted for repository architecture\nEnd the path with "/#.gz" to compress GeodesyML files with Gzip.')
parser.add_argument('--GeodesyMLdownloadList', '-gl', metavar='Comma separated site list  or network for GeodesyML download', type=str, default=None,
                    help='Download GeodesyML to specified direcory, nothing else will be done.\nSite should be in 9 characters, otehrwise use the --SiteEnd option.\nTo retrieve all stations from a network, use "%%" special key before network name like "%%RENAG".\n--GeodesyMLoutDirectory option is mandatory, --GeodesyML and --SiteEnd options are optional.\nie: ./gml2rnx.py -g M3G -gl "NICE00FRA,GINA00FRA,CLFD00FRA" -gd ./%%Y/#.gz')
parser.add_argument('--OutType', '-t', choices=['rnx', 'rnz', 'crx', 'crz'], default=None,
                    help='Select the output Rinex compression type.\nBy default it will use the same compression as input file.')
parser.add_argument('--OutDirectory', '-o', metavar='new Rinex destination directory', type=str, default=None,
                    help='Output directory path to write new Rinex files.\nDate POSIX format accepted for repository architecture.\n"#INTERVAL" and "#DURATION" special key and also their lowercase values can be use to complete the path.\nie: ./gml2rnx.py -o /DATA/#interval_#duration/%%Y/%%j/ SITE00FRA*.crx.gz')
parser.add_argument('--ForceOverwrite', '-f', action='store_true', default=False,
                    help='Overwrite output Rinex file.\nBy default, ".new" extension will be added.\nCaution: if no output specified, it will overwrite source Rinex file.')
parser.add_argument('--Decimate', '-d', action='store_true', default=False,
                    help='Decimate and concatenate Rinex files to daily 30sec files')
parser.add_argument('--Summary', '-s', action='store_true', default=False,
                    help='Compute satellites observations summary.\nAdd # OF SATELLITES and PRN / # OF OBS section in Rinex header.')
parser.add_argument('--SummaryPrint', '-sp', action='store_true', default=False,
                    help='Compute and print satellites observations summary.\nNothing else will be done except when using --Summary option in the same time')
parser.add_argument('--ChecksumMd5', '-cm', action='store_true', default=False,
                    help='Compute and print uncompressed and compressed MD5 checksum.\nOutput will be: FILE_PATH,UNCOMPRESSED_MD5,COMPRESSED_MD5 .')
parser.add_argument('--OverwriteReceiver', '-or', action='store_true', default=False,
                    help='Overwrite receiver metadata in Rinex header.\nBy default, the REC # / TYPE / VERS section is not modified.')
parser.add_argument('--OverwriteAntenna', '-oa', action='store_true', default=False,
                    help='Overwrite antenna metadata in Rinex header.\nBy default, the ANT # / TYPE section is not modified.')
parser.add_argument('--KeepXYZ', '-kx', action='store_true', default=False,
                    help='Keep the aproximate position coordinates in Rinex header.\nBy default, the APPROX POSITION XYZ section is modified.')
parser.add_argument('--KeepEccentricity', '-ke', action='store_true', default=False,
                    help='Keep the antenna eccentricity in Rinex header.\nBy default, the ANTENNA: DELTA H/E/N section is modified.')
parser.add_argument('--KeepMarkerName', '-kmn', action='store_true', default=False,
                    help='Keep the marker name in Rinex header.\nBy default it takes 9 characters site name.')
parser.add_argument('--RemoveHistory', '-rh', action='store_true', default=False,
                    help='Overwrite metadata Rinex header without saving history.\nBy default, original line is inserted in comment after modification in the header.')
parser.add_argument('--Comment', '-c', metavar='"max 60 characters string"', type=str, nargs='+',
                    help='Add comment line in Rinex header')
parser.add_argument('--RunBy', '-rb', metavar='{dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string', type=str, default='dataCenter',
                    help='Select the RUN BY Rinex header section from metadata list.\nBy default it takes primary datacenter from metadata.\nYou can also fill custom string, empty value will keep Rinex header entry.')
parser.add_argument('--Observer', '-ob', metavar='{dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string', type=str, default='onSiteContact',
                    help='Select the OBSERVER Rinex header section from metadata list.\nBy default it takes on-site primary contact name.\nYou can also fill custom string, empty value will keep Rinex header entry.')
parser.add_argument('--Agency', '-ag', metavar='{dataCenter, preparedBy, onSiteContact, onSiteAgency, ownerContact, ownerAgency} or custom string', type=str, default='ownerAgency',
                    help='Select the AGENCY Rinex header section from metadata list.\nBy default it takes responsible agency abbreviation.\nYou can also fill custom string, empty value will keep Rinex header entry.')
parser.add_argument('--SiteEnd', '-se', metavar='Last 5ch of 9ch Sitename', type=str, default='00FRA',
                    help='For GeodesyML download, 9 characters sitename is requied.\nThis option is needed to complete the 4 characters written in Rinex2 filename.\nBy default, 00FRA will be used.')
parser.add_argument('--ForcedMetadata', '-fm', metavar='"RUN BY, MARKER NAME, MARKER NUMBER, OBSERVER, AGENCY, REC #, REC TYPE, REC VERS, ANT #, ANT TYPE, X, Y, Z, H, E, N" or "keep"', type=str,
                    help='Force Rinex header metadata values.\nBe carefull, MARKER NAME section will also change the output filename even multiples files are processed.\nValues are comma separated, empty string will keep Rinex header entry.\nUse of "keep" parameter will NOT modifiy header metadata.\nThis option implies no M3G, DGW or GeodesyML metadata retrievement.\n--GeodesyML, --OverwriteReceiver, --OverwriteAntenna, --KeepXYZ, --KeepEccentricity, --KeepMarkerName, --RunBy, --Observer, and --Agency options will be ignored.\nie: ./gml2rnx.py -fm "RENAG Automatic,,,OREME-Team,RESIF,,,,CR520021307,ASH701945D_M    NONE,4591090.1844,258415.2417,4406201.0377,0,0,0" SITE00FRA*.crx.gz')
args = parser.parse_args()


#check Gzip compression key in GeodesyML output directory path
if args.GeodesyMLoutDirectory and "/#.gz"==args.GeodesyMLoutDirectory[-5:]:
    args.GeodesyMLoutDirectory=args.GeodesyMLoutDirectory[:-5]
    gzip_gml=True
else:
    gzip_gml=False

#download GeodesyML mode
if args.GeodesyMLdownloadList:
    #keys of network
    if '%' in args.GeodesyMLdownloadList:
        #get network site list from M3G API
        if args.GeodesyML == 'M3G':
            args.GeodesyMLdownloadList=get_M3G_network_sites(args.GeodesyMLdownloadList[1:])
        #get network site list from DGW API
        elif args.GeodesyML == 'DGW':
            args.GeodesyMLdownloadList=get_DGW_network_sites(args.GeodesyMLdownloadList[1:])
        #get network site list from path
        else:
            print("ERROR: Cannot get network site list from GeodesyML repository.\nPlease use --GeodesyML with M3G or DGW option.", file=stderr, flush=True)
            exit()
    #loop for all sites
    for site in args.GeodesyMLdownloadList.split(','):
        #check GeodesyML out directory
        if not args.GeodesyMLoutDirectory:
            if not args.Quiet: print("NOTICE: GeodesyML output directory path is not provided. Please use --GeodesyMLoutDirectory option")
            exit()
        #check site name
        if len(site)==4:
            site=site.upper()+args.SiteEnd
        elif len(site)==9:
            site=site.upper()
        else:
            if not args.Quiet: print("NOTICE: Site is malformated, skipping:",site)
            continue
        #get GeodesyML from M3G API
        if args.GeodesyML == 'M3G':
            gml=get_M3G_geodesyML(site)
            if not gml:
                continue
        #get GeodesyML from DGW API
        elif args.GeodesyML == 'DGW':
            gml=get_DGW_geodesyML(site)
            if not gml:
                continue
        #get GeodesyML from path
        else:
            gml=get_geodesyML(site,args.GeodesyML)
            if not gml:
                continue
        #get metadata from GeodesyML
        gml_dict=get_metadata(gml)
      
        #POSIX date format in output directory string
        if '%' in args.GeodesyMLoutDirectory:
            args.GeodesyMLoutDirectory=datetime.strptime(gml_dict["geo:datePrepared"],"%Y-%m-%d").strftime(args.GeodesyMLoutDirectory)
            
        #the specified directory doesn't exist
        if not path.isdir(args.GeodesyMLoutDirectory):
            #create new output directory
            makedirs(args.GeodesyMLoutDirectory, exist_ok=True)
        #write geodesyML using SITE00XXX_YYYYMMDD.xml file format
        gmlpath=path.join(args.GeodesyMLoutDirectory,site+'_'+gml_dict["geo:datePrepared"].replace('-','')[:8]+".xml")
        #compress GeodesyML with Gzip
        if gzip_gml:
            gmlpath+=".gz"
            gml=gml.encode("utf-8","replace")
            if pgzip_BS:
                gml=pgzip.compress(gml,blocksize=pgzip_BS)
            else:
                gml=gzip.compress(gml)
            open(gmlpath,'wb').write(gml)
        else:
            open(gmlpath,'w').write(gml)
        print(gmlpath)

#initiate metadata dictionary
gml_dict=dict()
#initiate last decimated rinex dictionary
last_dec=dict()
#sort Rinex
args.Rinex.sort(key=lambda x: x.name)
#list files from input directory and add into Rinex
if args.InputPath:
    if path.isdir(args.InputPath):
        for f in sorted(glob(path.join(args.InputPath,"*"))):
            args.Rinex.append(f)
    else:
        for f in sorted(glob(args.InputPath)):
            args.Rinex.append(f)
#add a final loop for decimation processing
if args.Decimate:
    args.Rinex.append("END")
#main loop for all rinex files to be proceed
for Rinex in args.Rinex:
    #case of decimation end loop: process last file
    if isinstance(Rinex, str) and Rinex == "END":
        rnx_dict=last_dec
        args.Decimate=False
    else:
        #convert filepath to filehandle
        if isinstance(Rinex, str):
            Rinex=open(Rinex,"rb") 
        #get metadata and content from rinex file
        rnx_dict=get_rinex(Rinex)
        #close file
        Rinex.close()
        #next file if not Rinex
        if not rnx_dict:
            continue
        #Show satelite observation statistics and skip other functions
        if args.SummaryPrint and not args.Summary:
           print(Rinex.name)
           rnx_dict=satstat(rnx_dict,args.OutType)
           printsatstat(rnx_dict)
           continue

    #Decimate mode
    if args.Decimate:
        if rnx_dict["interval"]==30 and rnx_dict["duration"]=='01D':
            if not args.Quiet: print("NOTICE: Skip", rnx_dict["name"], ": Already a 30s/24h Rinex file", file=stderr, flush=True)
            continue
        if not args.Quiet: print("NOTICE: Decimate", rnx_dict["name"], file=stderr, flush=True)
        dec=decimate(rnx_dict,args.Summary)
        #check new file is the same date, same site and same rinex version
        if last_dec and last_dec["date"].date() == dec["date"].date() and last_dec["site"] == dec["site"] and last_dec["type"][3] == dec["type"][3]:
            last_dec=concatenate(last_dec,dec)
            continue
        elif last_dec:
            #new day or site -> process last concatened file
            rnx_dict=last_dec
            last_dec=dec
        else:
            #first run
            last_dec=dec
            continue
    elif args.Summary:
        #compute satelite observation statistics (already done during decimate mode)
        rnx_dict=satstat(rnx_dict,args.OutType)

    #Forced mode
    if args.ForcedMetadata:
        if args.ForcedMetadata == "keep":
            args.ForcedMetadata = ",,,,,,,,,,,,,,,"
        #get metadata from csv string
        header_val=args.ForcedMetadata.split(',')
        if len(header_val) == 16:
            #insert metadata into Rinex file content
            rnx_dict=rinex_insert(header_val[0],header_val[1],header_val[2],header_val[3],header_val[4],header_val[5],header_val[6],header_val[7],header_val[8],header_val[9],header_val[10],header_val[11],header_val[12],header_val[13],header_val[14],header_val[15],True,True,False,False,False,args.Summary,args.Comment,args.RemoveHistory,args.Quiet,rnx_dict)
            rnx_dict["new_site"]=header_val[1]
        else:
            print("ERROR: --ForcedMetadata string parameter is malformated. It must contain 16 comma separated values or keep.\nie: ./gml2rnx.py -fm \"RENAG Automatic,,,OREME-Team,RESIF,,,,CR520021307,ASH701945D_M    NONE,4591090.1844,258415.2417,4406201.0377,0,0,0\" SITE00FRA*.crx.gz", file=stderr, flush=True)
            continue

    #Normal mode
    else:
        #check metadata already download
        if rnx_dict["site"] not in gml_dict.keys():
            #get GeodesyML from M3G API
            if args.GeodesyML == 'M3G':
                gml=get_M3G_geodesyML(rnx_dict["site"])
                if not gml:
                    continue
            #get GeodesyML from DGW API
            elif args.GeodesyML == 'DGW':
                gml=get_DGW_geodesyML(rnx_dict["site"])
                if not gml:
                    continue
            #get GeodesyML from path
            else:
                gml=get_geodesyML(rnx_dict["site"],args.GeodesyML)
                if not gml:
                    continue
            #get metadata from GeodesyML
            gml_dict[rnx_dict["site"]]=get_metadata(gml)
            #store GeodesyML
            if args.GeodesyMLoutDirectory:
                #POSIX date format in output directory string
                if '%' in args.GeodesyMLoutDirectory:
                    args.GeodesyMLoutDirectory=datetime.strptime(gml_dict[rnx_dict["site"]]["geo:datePrepared"],"%Y-%m-%d").strftime(args.GeodesyMLoutDirectory)
                #the specified directory if a file
                if path.isfile(args.GeodesyMLoutDirectory):
                    print("ERROR: --GeodesyMLoutDirectory parameter is a file. It must be a directory path.", file=stderr, flush=True)
                    exit()
                #the specified directory doesn't exist
                if not path.isdir(args.GeodesyMLoutDirectory):
                    #create new output directory
                    makedirs(args.GeodesyMLoutDirectory, exist_ok=True)
                #write geodesyML using SITE00XXX_YYYYMMDD.xml file format
                gmlpath=path.join(args.GeodesyMLoutDirectory,rnx_dict["site"]+'_'+gml_dict[rnx_dict["site"]]["geo:datePrepared"].replace('-','')[:8]+".xml")
                #compress GeodesyML with Gzip
                if gzip_gml:
                    gmlpath+=".gz"
                    gml=gml.encode("utf-8","replace")
                    if pgzip_BS:
                        gml=pgzip.compress(gml,blocksize=pgzip_BS)
                    else:
                        gml=gzip.compress(gml)
                    open(gmlpath,'wb').write(gml)
                else:
                   open(gmlpath,'w').write(gml)
                    

        #select receiver metadata session according to rinex date
        rec_index=-1
        for i in range(len(gml_dict[rnx_dict["site"]]["receiver_tab"])):
            if (not gml_dict[rnx_dict["site"]]["receiver_tab"][i]["geo:dateRemoved"] or datetime.strptime(gml_dict[rnx_dict["site"]]["receiver_tab"][i]["geo:dateRemoved"],"%Y-%m-%dT%H:%M:%SZ") >= rnx_dict["date"]) and datetime.strptime(gml_dict[rnx_dict["site"]]["receiver_tab"][i]["geo:dateInstalled"],"%Y-%m-%dT%H:%M:%SZ") <= rnx_dict["date"]:
                rec_index=i
        if rec_index==-1:
            print("ERROR: Cannot found", rnx_dict["site"], "receiver metadata according to rinex date:", rnx_dict["date"], file=stderr, flush=True)
            continue
        #select antenna metadata session according to rinex date
        ant_index=-1
        for i in range(len(gml_dict[rnx_dict["site"]]["antenna_tab"])):
            if (not gml_dict[rnx_dict["site"]]["antenna_tab"][i]["geo:dateRemoved"] or datetime.strptime(gml_dict[rnx_dict["site"]]["antenna_tab"][i]["geo:dateRemoved"],"%Y-%m-%dT%H:%M:%SZ") >= rnx_dict["date"]) and datetime.strptime(gml_dict[rnx_dict["site"]]["antenna_tab"][i]["geo:dateInstalled"],"%Y-%m-%dT%H:%M:%SZ") <= rnx_dict["date"]:
                ant_index=i
        if ant_index==-1:
            print("ERROR: cannot found", rnx_dict["site"], "antenna metadata according to rinex date:",rnx_dict["date"], file=stderr, flush=True)
            continue
        #check Runby argument
        if 'geo:'+args.RunBy not in gml_dict[rnx_dict["site"]].keys():
            gml_dict[rnx_dict["site"]]['geo:'+args.RunBy]=args.RunBy
        #check Observer argument
        if 'geo:'+args.Observer not in gml_dict[rnx_dict["site"]].keys():
            gml_dict[rnx_dict["site"]]['geo:'+args.Observer]=args.Observer
        #check Agency argument
        if 'geo:'+args.Agency not in gml_dict[rnx_dict["site"]].keys():
            gml_dict[rnx_dict["site"]]['geo:'+args.Agency]=args.Agency
        #insert metadata into Rinex file conent
        rnx_dict=rinex_insert(gml_dict[rnx_dict["site"]]['geo:'+args.RunBy],rnx_dict["site"],gml_dict[rnx_dict["site"]]["geo:iersDOMESNumber"],gml_dict[rnx_dict["site"]]['geo:'+args.Observer],gml_dict[rnx_dict["site"]]['geo:'+args.Agency],gml_dict[rnx_dict["site"]]["receiver_tab"][rec_index]["geo:manufacturerSerialNumber"],gml_dict[rnx_dict["site"]]["receiver_tab"][rec_index]["geo:igsModelCode"],gml_dict[rnx_dict["site"]]["receiver_tab"][rec_index]["geo:firmwareVersion"],gml_dict[rnx_dict["site"]]["antenna_tab"][ant_index]["geo:manufacturerSerialNumber"],gml_dict[rnx_dict["site"]]["antenna_tab"][ant_index]["geo:igsModelCode"],gml_dict[rnx_dict["site"]]["geo:X"],gml_dict[rnx_dict["site"]]["geo:Y"],gml_dict[rnx_dict["site"]]["geo:Z"],gml_dict[rnx_dict["site"]]["antenna_tab"][ant_index]["geo:marker-arpUpEcc."],gml_dict[rnx_dict["site"]]["antenna_tab"][ant_index]["geo:marker-arpEastEcc."],gml_dict[rnx_dict["site"]]["antenna_tab"][ant_index]["geo:marker-arpNorthEcc."],args.OverwriteReceiver,args.OverwriteAntenna,args.KeepXYZ,args.KeepEccentricity,args.KeepMarkerName,args.Summary,args.Comment,args.RemoveHistory,args.Quiet,rnx_dict)

    #convert and store new modified Rinex
    outfile=write_output(args.ForceOverwrite,args.OutDirectory,args.OutType,args.ChecksumMd5,rnx_dict)
    if outfile:
        print(outfile)
        if args.SummaryPrint:
            printsatstat(rnx_dict)

#notice to install pgzip
if not args.Quiet and ((args.OutType is not None and args.OutType[2]=='z') or gzip_gml) and pgzip_BS==0: print("NOTICE: You must install pgzip library to speedup the tool (ie. pip3 install pgzip).", file=stderr, flush=True)
