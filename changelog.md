# 2023-04-18
Version 1.0.0
# 2023-07-24
Version 1.1.0 
* Provide history of material modification (antenna and receiver) in RINEX header comments
* Add number of observations recoded using summary option
* Fix decimation crash when constellation disapear
* Fix outrageous notifications about Pgzip installation 
# 2023-09-15
Version 1.2.0 
* Add data stream source '_S_' support in RINEX3 filename.
* Add NAVIC satellite system support.
* Add program version, function and date of processing in header.
* Switch --KeepAntenna to --OverwriteAntenna option to keep antenna metadata by default.
* Add --RemoveHistory option, by default insert orginal lines in comment after modification in header.
# 2024-06-11
Version 1.2.1
* Fix decimation crash on empty observation RINEX files.
* Add --InputPath option to allow processing of huge amount of files.
* Modify the filename output with the MARKER NAME using ForcedMetadata option.
# 2024-06-14
Version 1.2.2
* Performance optimisation on multiple files processing
* Close filehandle after reading contents
* Use Pgzip library on compression only
# 2024-07-31
Vesion 1.2.3
* Fix Antenna modification in --ForcedMetadata option
* Add MARKER NUMBER section if not present in Header
* Handle .Z compression
# 2024-09-24
Version 1.2.4
* Fix decimation/concatenation bugs
* Add wildcard in --InputPath option
